$(document).on("ready", function(){
if(Cookie.get('smybbes_picker')) {
	colorize = Cookie.get('smybbes_picker');	
    $('.logo, .thead, .tfoot, #copyright').css('background', Cookie.get('smybbes_picker')+' url(images/dntadvantage/maintitle_pattern.png) no-repeat 0 0');
	$('.announcement, li.active').css({'background': Cookie.get('smybbes_picker'),'color': '#fff'});
	$('.forum_on, .subforum_minion, .links').css('color', Cookie.get('smybbes_picker'));
	$('#portal #port a:link, #portal #port a:visited, #foros #foro a:link, #foros #foro a:visited, #members #member a:link, #members #member a:visited, #ayuda #help a:link, #ayuda #help a:visited, #calendar #cale a:link, #calendar #cale a:visited, #search #busq a:link, #search #busq a:visited').css({'color':Cookie.get('smybbes_picker'), 'border-bottom':'2px solid '+Cookie.get('smybbes_picker')});
	$("ul.tabs li:first").css("background", Cookie.get('smybbes_picker')).show();
	$(".tab_content:first").addClass("active").show();
	$("ul.tabs li").click(function() {
	$("ul.tabs li").removeClass("active");		
	$("ul.tabs li").css("background", "#242424");
	$(this).css("background", Cookie.get('smybbes_picker')).addClass("active");
	$(".tab_content").hide();
	var activeTab = $(this).find("a").attr("href");
	$(activeTab).fadeIn();
	return false;
	});
}
else{
	colorize = '#0f7789';
	$("ul.tabs li:first").css("background", "#0f7789").show();
	$(".tab_content:first").addClass("active").show();
	$("ul.tabs li").click(function() {
	$("ul.tabs li").removeClass("active");
	$("ul.tabs li").css("background", "#242424");
	$(this).css("background", "#0f7789").addClass("active");
	$(".tab_content").hide();
	var activeTab = $(this).find("a").attr("href");
	$(activeTab).fadeIn();
	return false;
	});
}
if (typeof memug !== 'undefined') {
	if(memug == 1)
	{
		$(".id3").show();		
		$(".id4").show();		
	}
	else
	{
		$(".id3").hide();
		$(".id4").hide();		
	}	
}
$('#colorSelector').ColorPicker({
color: colorize,
onShow: function (colpkr) {
	$(colpkr).fadeIn(500);
	return false;
},
onHide: function (colpkr) {
	$(colpkr).fadeOut(500);
	return false;
},
onChange: function (hsb, hex, rgb) {		
	$('.logo, .thead, .tfoot, #copyright').css('background', '#' + hex + ' url(images/dntadvantage/maintitle_pattern.png) no-repeat 0 0');
	$('.announcement, li.active').css({'background':'#' + hex, 'color':'#fff'});
	$('#portal #port a:link, #portal #port a:visited, #foros #foro a:link, #foros #foro a:visited, #members #member a:link, #members #member a:visited, #ayuda #help a:link, #ayuda #help a:visited, #calendar #cale a:link, #calendar #cale a:visited, #search #busq a:link, #search #busq a:visited').css({'color':'#'+hex, 'border-bottom':'2px solid #'+hex});
	$('.forum_on, .subforum_minion, .links').css('color', '#'+hex);
	$('smybbes_picker').val('#'+hex);
    Cookie.set('smybbes_picker', '#' + hex, { expires: 365 });		
}});
});