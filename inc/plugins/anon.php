<?php
/**
 * MyBB 1.4
 * Copyright © 2008 MyBB Group, All Rights Reserved
 *
 * Website: http://www.mybboard.net
 * License: http://www.mybboard.net/about/license
 *
 * $Id: anon.php 4304 2009-01-02 01:11:56Z chris $
 */
 
// Disallow direct access to this file for security reasons
if(!defined("IN_MYBB"))
{
    die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");
}

$plugins->add_hook("datahandler_post_insert_post", "anon_post");
$plugins->add_hook("datahandler_post_insert_thread", "anon_thread");
$plugins->add_hook("datahandler_post_insert_thread_post", "anon_post");

function anon_info()
{
    /**
     * Array of information about the plugin.
     * name: The name of the plugin
     * description: Description of what the plugin does
     * website: The website the plugin is maintained at (Optional)
     * author: The name of the author of the plugin
     * authorsite: The URL to the website of the author (Optional)
     * version: The version number of the plugin
     * guid: Unique ID issued by the MyBB Mods site for version checking
     * compatibility: A CSV list of MyBB versions supported. Ex, "121,123", "12*". Wildcards supported.
     */
    return array(
        "name"            => "Anonymous",
        "description"    => "A plugin that attributes all user posts in a certain forum to user X (anonymous)",
        "website"        => "hiyowei.com/ud",
        "author"        => "Joe Yuan",
        "authorsite"    => "hiyowei.com",
        "version"        => "1.1",
        "guid"             => "",
        "compatibility" => "*"
    );
}

function anon_activate()
{
	//require "../inc/adminfunctions_templates.php";
	//global $db, $mybb;
	//$anonymous_group = array(
	//	"gid" = "",
	//	"name" = "anonymous",
	//	"title" = "anonumous forum",
	//	"description" = "enable anonymously posting",
	//	"disporder" = "100",
	//	"isdefault" = "no",
	//);
	//$db->insert_query(TABLE_PREFIX."settinggroups", $anonymous_group);
	//$gid = $db->insert_id();

	//$anonymous_setting = array(
	//	"sid"	=> "NULL",
	//	"name"	=> "anonymous setting",
	//	"title"	=> "Enable",
	//	"description"	=> "Would you like to enable PLUGINNAME?",
	//	"optionscode"	=> "yesno", 
	//	"value"	=> "yes",
	//	"disporder"	=> "1",
	//	"gid"	=> intval($gid),
	//);
	//$db->insert_query(TABLE_PREFIX."settings", $anonymous_setting)
	
}

function anon_deactivate()
{
	//equire '../inc/adminfunctions_templates.php';
	//global $db;
	//
	//$db->query("DELETE FROM ".TABLE_PREFIX."settings WHERE name IN('anonymous setting', 'anonymous setting')");
	//$db->query("DELETE FROM ".TABLE_PREFIX."settinggroups WHERE name='anonymous'"); 
}


function anon_post(&$insert_data)
{
    if($insert_data->post_insert_data['fid'] == 19)
    {
        $insert_data->post_insert_data['uid'] = "22";
        $insert_data->post_insert_data['username'] = "Anonymous";
    }
    
    return $insert_data;
} 

function anon_thread(&$insert_data)
{
    if($insert_data->thread_insert_data['fid'] == 19)
    {
        $insert_data->thread_insert_data['uid'] = "22";
        $insert_data->thread_insert_data['username'] = "Anonymous";
    }
    
    return $insert_data;
} 
?>
