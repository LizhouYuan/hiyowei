<?php
/**
 * MyBB 1.2
 * Copyright © 2006 MyBB Group, All Rights Reserved
 *
 * Website: http://www.mybboard.net
 * License: http://www.mybboard.net/eula.html
 *
 * $Id: hi.php 2932 2007-03-10 05:48:55Z chris $
 */

// Disallow direct access to this file for security reasons
if(!defined("IN_MYBB"))
{
	die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");
}

$plugins->add_hook("pre_output_page", "hi_world");
$plugins->add_hook("postbit", "hi_world_postbit");

function hi_info()
{
	return array(
		"name"			=> "Hiyowei",
		"description"	=> "A sample plugin that prints hi world and changes the content of each post to 'Hello world!'",
		"website"		=> "http://www.google.com",
		"author"		=> "hiyowei",
		"authorsite"	=> "http://",
		"version"		=> "1.0",
	);
}

function hi_activate()
{
}

function hi_deactivate()
{
}

function hi_world($page)
{
	$page = str_replace("<div id=\"content\">", "<div id=\"content\"><p>Hello World!<br />This is a sample Plugin (which can be disabled!) that displays this message on all pages.</p>", $page);
	return $page;
}

function hi_world_postbit($post)
{
	$post['message'] = "<strong>Hi yo wei!</strong><br /><br />{$post['message']}";
}

?>
