<?php
/**
 * Update Cached Styles
 * 
 * PHP Version 5
 * 
 * @category MyBB_18
 * @package  Up_Styles
 * @author   chack1172 <NBerardozzi@gmail.com>
 * @license  https://creativecommons.org/licenses/by-nc/4.0/ CC BY-NC 4.0
 * @link     http://www.chack1172.altervista.org/Projects/MyBB-18/Update-cached-styles.html
 */

if (!defined("IN_MYBB")) {
    die('Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.');
}

$plugins->add_hook('global_intermediate', 'up_styles_change');

/**
 * Return the plugin description
 * 
 * @return array plugin info
 */
function up_styles_info()
{
    global $lang;
    $lang->load('up_styles');
    
    return array(
        'name'          => $lang->up_styles,
        'description'   => $lang->up_styles_desc,
        'website'       => $lang->up_styles_url,
        'author'        => 'chack1172',
        'authorsite'    => $lang->up_styles_author,
        'version'       => '1.1',
        'compatibility' => '18*',
        'codename'      => 'up_styles'
    );
}

/**
 * Activate the plugin
 */
function up_styles_activate()
{
}

/**
 * Deactivate the plugin
 */
function up_styles_deactivate()
{   
}

/**
 * Add the last edit time to css files
 */
function up_styles_change()
{
    global $stylesheets, $db, $mybb;

    if (isset($mybb->user['theme'])) {
        $where = 'tid = '.$mybb->user['theme'];
    } else {
        $query = $db->simple_select('themes', 'tid', 'def=1');
        $id = $db->fetch_array($query);
        $where = 'tid = '.$id['tid'];
    }

    $query = $db->query('SELECT sid, cachefile, lastmodified FROM '.TABLE_PREFIX.'themestylesheets WHERE '.$where.'');
    if ($db->num_rows($query) > 0) {
        $list = array();
        while ($style = $db->fetch_array($query)) {
            $list[$style['cachefile']] = $style['lastmodified'];
            $list[$style['sid']] = $style['lastmodified'];
        }
    }

    $styles = explode("\n", $stylesheets);
    foreach ($styles as $style) {
        if (preg_match("/\<link type\=\"text\/css\" rel\=\"stylesheet\" href\=\"(.*?)\" \/\>/", $style, $file)) {
            $base = str_replace($mybb->settings['bburl'].'/', '', $file[1]);
            if (preg_match("/css.php\?stylesheet=([0-9]+)/", $base, $id)) {
                if (isset($list[$id[1]])) {
                    $name = $file[1].'&lastedit='.$list[$id[1]];
                    $stylesheets = str_replace($file[1], $name, $stylesheets);
                }
            } else {
                $name = basename(str_replace('.min.css', '.css', $base));
                if (isset($list[$name])) {
                    $name = $file[1].'?lastedit='.$list[$name];
                    $stylesheets = str_replace($file[1], $name, $stylesheets);
                }
            }
        }
    }
}