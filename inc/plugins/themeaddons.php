<?php

// Disallow direct access to this file for security reasons
if(!defined("IN_MYBB"))
{
	die("Direct initialization of this file is not allowed.");
}

function themeaddons_info() {
	return array(
		"name"			=> "Niezbędne funkcje do szablony MyFlatBoard 3",
		"description"	=> "Dodaje wszystkie funkcję do szablonu MyFlatBoard 3",
		"author"		=> "FilipCichorek.pl, Krugerz.ovh(Zmiana koloru stylu)",
		"authorsite"	=> "htts://filipcichorek.pl",
		"version"		=> "1.0",
		"codename"		=> "themeaddons",
		"compatibility" => "*"
	);
}

$plugins->add_hook("global_start", "urlavatar_global");
$plugins->add_hook("global_start", "useravatar_global");
$plugins->add_hook("global_start", "dark_global");
$plugins->add_hook("index_start", "dark_index");
$plugins->add_hook("global_start", "dark_button");

function urlavatar_global() {
    global $db, $mybb, $urlavatar, $lravatar;
	$query = $db->write_query("SELECT avatar FROM mybb_users ORDER BY `uid` DESC LIMIT 1");
    if ($query->num_rows == 1) {
        $result = $db->fetch_array($query);   
        if (empty($result['avatar'])) {
            if(isset($mybb->cookies["dark"])) {
                $urlavatar = 'images/MyFlatBoard3/default_avatar_dark.png';
            } else {
                $urlavatar = 'images/MyFlatBoard3/default_avatar_light.png';  
            }
        } else {
            $urlavatar = $result['avatar'];
        }
        $lravatar = "<img src='{$urlavatar}' alt='avatar' />";
    }
}

function useravatar_global() {
    global $mybb, $useravatar;
    if (empty($mybb->user['avatar'])) {
            if(isset($mybb->cookies["dark"])) {
                $useravatar = 'images/MyFlatBoard3/default_avatar_dark.png';
            } else {
                $useravatar = 'images/MyFlatBoard3/default_avatar_light.png';  
            }
    } else {
        $useravatar = "{$mybb->user['avatar']}";
    }
     
}

function dark_global() {
	global $mybb, $mfb3_dark_css, $mfb3_logo_dark, $mfb3_unread_display;
	if(isset($mybb->cookies["dark"])) {
		$mfb3_dark_css = "<link type='text/css' rel='stylesheet' href='$mybb->asset_url/images/MyFlatBoard3/dark.css'/>";
        $mfb3_logo_dark = "$mybb->asset_url/images/MyFlatBoard3/logo_dark.png";
	} else {
        $mfb3_dark_css = "";
        $mfb3_logo_dark = "$mybb->asset_url/images/MyFlatBoard3/logo.png"; 
    }
    if (empty($mybb->user['pms_unread'])) {
        $mfb3_unread_display = " dnone";
    } else {
        $mfb3_unread_display = "";
    }  
}

function dark_index() {
	global $mybb, $lang, $darkbutton;
    $lang->load("MyFlatBoard3");
	if($mybb->input['action'] == "change_theme_mode" && verify_post_check($mybb->input['my_post_key'], false)) {
		if(isset($mybb->cookies["dark"])) {
			my_unsetcookie("dark");
			redirect("index.php", $lang->mfb3_changed_light);
		} else { 
			my_setcookie("dark", "1"); 
			redirect("index.php", $lang->mfb3_changed_dark);
		}
		exit;
	}
}

function dark_button() {
    global $mybb, $lang, $darkbutton;
    $lang->load("MyFlatBoard3");
    $darkbutton = "<li><a href='{$mybb->settings['bburl']}/index.php?action=change_theme_mode&my_post_key={$mybb->post_code}'  class='change__color' data-toggle='tooltip' data-placement='bottom' title='$lang->mfb3_change_color'></a></li>";
}
?>