<?php
/**
 * Update Cached Styles
 * 
 * PHP Version 5
 * 
 * @category MyBB_18
 * @package  Up_Styles
 * @author   chack1172 <NBerardozzi@gmail.com>
 * @license  https://creativecommons.org/licenses/by-nc/4.0/ CC BY-NC 4.0
 * @link     http://www.chack1172.altervista.org/Projects/MyBB-18/Update-cached-styles.html
 */

$l['up_styles'] = "Update cached styles";
$l['up_styles_desc'] = "Every time a stylesheet is updated, it will also be updated if it is saved in the browser cache.";
$l['up_styles_url'] = "http://www.chack1172.altervista.org/Projects/MyBB-18/Update-cached-styles.html";
$l['up_styles_author'] = "http://www.chack1172.altervista.org/?language=english";