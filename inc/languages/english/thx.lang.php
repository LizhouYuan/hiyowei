<?php 
/**
 * Likes Plugin - english
 */

$l['thx_main'] = "Likes";
$l['thx_givenby'] = "Likes given by:";
$l['thx_likeed_count'] = "Given {1} like(s) in {2} post(s)";
$l['thx_like'] = "Likes:";
$l['thx_remove'] = "Remove Likes";
$l['thx_comma'] = " , ";
$l['thx_dir'] = "ltr";
$l['thx_processing'] = 'Processing...';
?>
