<?php
 /**
 *    MyFlatBoard v3 English Language Pack
 *  Copyright © 2018 Filip 'SimLay' Cichorek
 *         Website: FilipCichorek.pl
 **/


/** Zmiana Koloru **/
 $l['mfb3_changed_dark'] = "Changed on dark version.";
 $l['mfb3_changed_light'] = "Changed on light version.";
 $l['mfb3_change_color'] = "Change Color";

/** Menu **/
 $l['mfb3_memberlist'] = "Users List";
 $l['mfb3_search'] = "Search";
 $l['mfb3_stats'] = "Stats";
 $l['mfb3_search_text'] = "Search...";

/** Panel Usera **/
 $l['mfb3_welcome'] = "You have account?";
 $l['mfb3_login_button'] = "Log in";
 $l['mfb3_register_button'] = "Register";
 $l['mfb3_messeges'] = "Messages";
 $l['mfb3_accountsetings_title'] = "Account settings";
 $l['mfb3_showprofile'] = "Show my profile";
 $l['mfb3_editprofile'] = "Edit profile";
 $l['mfb3_editsignature'] = "Edit signature";
 $l['mfb3_changepassword'] = "Change password";
 $l['mfb3_friendlist'] = "Friend list";
 $l['mfb3_settings'] = "Settings";
 $l['mfb3_content_title'] = "Content";
 $l['mfb3_myattachments'] = "My attachments";
 $l['mfb3_followedcontent'] = "Observed content";
 $l['mfb3_mycontent'] = "My content";
 $l['mfb3_logout'] = "Log out";
 // $l['mfb3_shouts'] = "shouts";

/** Profil **/
 $l['mfb3_profile_status_online'] = "is offline";
 $l['mfb3_profile_status_offline'] = "is online";
 $l['mfb3_profile_send_pm'] = "Send PM";
 $l['mfb3_profile_posts'] = "Posts";
 $l['mfb3_profile_threads'] = "Threads";
 $l['mfb3_profile_rep'] = "Reputation";
 $l['mfb3_profile_regdate'] = "Register Date";
 $l['mfb3_profile_bday'] = "birthday";
 $l['mfb3_profile_user_activity'] = "User Activity";
 $l['mfb3_profile_user_info'] = "Informations";
 $l['mfb3_profile_usergroup'] = "User Group:";

/** Statstyki **/
 $l['mfb3_posts'] = "posts";
 $l['mfb3_threads'] = "threads";
 $l['mfb3_replies'] = "replies";
 $l['mfb3_views'] = "views";
 $l['mfb3_users'] = "members";
 $l['mfb3_newestmember'] = "newest member";

/** Stopka **/
 $l['mfb3_poweredby'] = "Powered by";
 $l['mfb3_engine'] = "Forum Engine";
 $l['mfb3_translation'] = "{$lang->powered_by}";
 $l['mfb3_skinby'] = "Created by";
 $l['mfb3_simlayslogan'] = "Mybb themes for you!";
?>