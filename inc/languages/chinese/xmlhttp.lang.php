<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['no_new_subject'] = "您没有输入新标题。";
$l['post_moderation'] = "您的帖子正在等待审核中。";
$l['thread_moderation'] = "您的主题正在审核中。";
$l['post_doesnt_exist'] = "指定的帖子不存在。";
$l['thread_doesnt_exist'] = "指定的主题不存在。";
$l['thread_closed_edit_subjects'] = "这个主题已关闭, 您不可以修改标题。";
$l['no_permission_edit_subject'] = "您没有权限修改这个主题的标题。";
$l['thread_closed_edit_message'] = "这个主题已关半, 您不可以修改里面的内容。";
$l['no_permission_edit_post'] = "您没有权限编辑这个帖子。";
$l['edit_time_limit'] = "您只能在帖子发布 {1} 分钟后再编辑。";
$l['postbit_edited'] = "这个帖子最后修改于: {1} 由";
$l['postbit_editreason'] = "编辑理由";
$l['save_changes'] = "保存改动";
$l['cancel_edit'] = "取消编辑";
$l['answer_valid_not_exists'] = "您尝试回答的问题不存在";
$l['captcha_not_exists'] = "您尝试刷新的验证码图片不存在。";
$l['captcha_valid_not_exists'] = "您尝试检查的验证码图片似乎不存在。";
$l['captcha_does_not_match'] = "您输入的图片验证码不正确, 请准确地输入图片中显示的验证码。";
$l['captcha_matches'] = "您输入的图片验证码正确有效。";
$l['answer_does_not_match'] = "您输入的回答不正确";
$l['banned_username'] = "您输入的用户名已经被管理员禁用";
$l['banned_characters_username'] = "您的用户名包含了一个或多个无效字符";
$l['complex_password_fails'] = "密码必须由大写字母、小写字母和数字组合而成";
$l['username_taken'] = "{1} 已经被其它会员先注册掉了";
$l['username_available'] = "{1} 是可用的";
$l['invalid_username'] = "{1} 不是有效的注册用户名";
$l['valid_username'] = "{1} 是有效的推荐人。";
$l['buddylist_error'] = "似乎您的好友名单里还没有任何好友. 请在使用这个之前先添加一些。";
$l['close'] = "关闭";
$l['select_buddies'] = "选择好友";
$l['select_buddies_desc'] = "若要添加一个或多个您的好友作为收件人, 请在下面选择他们然后按确定。";
$l['selected_recipients'] = "选定的收件人";
$l['ok'] = "OK";
$l['cancel'] = "取消";
$l['online'] = "在线";
$l['offline'] = "离线";
$l['edited_post'] = "帖子修改";
$l['usergroup'] = "用户组";
