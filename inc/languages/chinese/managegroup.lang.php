<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['nav_group_management'] = "用户组管理 {1}";
$l['nav_join_requests'] = "加入请求";
$l['nav_group_memberships'] = "用户组成员";

$l['not_leader_of_this_group'] = "抱歉, 您不是这个用户组的组长之一。";
$l['invalid_group'] = "这个用户组不存在。";
$l['pending_requests'] ="未决的加入请求";
$l['num_requests_pending'] = "这个用户组里目前有 <strong>{1}</strong> 个未决的加入请求。";
$l['group_management'] = "用户组管理";
$l['members_of'] = "\"{1}\" 的成员";
$l['user_name'] = "用户名";
$l['contact'] = "联系";
$l['reg_date'] = "注册时间";
$l['post_count'] = "发表数";
$l['remove_selected'] = "把选定的用户移出此小组";
$l['add_member'] = "添加成员到 \"{1}\"";
$l['add_member_submit'] = "添加成员到小组";
$l['invite_member'] = "邀请成员到 \"{1}\"";
$l['invite_member_submit'] = "邀请成员到小组";
$l['join_requests'] = "加入请求";
$l['join_requests_title'] = "\"{1}\" 的加入请求";
$l['leader'] = "(组长)";
$l['reason'] ="理由";
$l['accept'] = "同意";
$l['ignore'] = "忽略";
$l['decline'] = "拒绝";
$l['action_requests'] = "确定执行";
$l['join_requests_moderated'] = "加入要求的审核动作已经执行完毕.<br />您将转入到请求列表页面。";
$l['no_requests'] = "这个用户组当前没有未决的加入请求。";
$l['no_users'] = "当前小组没有成员。";
$l['user_added'] = "这个用户已经被加到这个组里了。";
$l['users_removed'] = "选定的用户已经从该组移出。";
$l['group_no_members'] = "这个用户组当前没有成员.<br />若要返回用户组管理页面, 点击<a href=\"usercp.php?action=usergroups\">这儿</a>。";
$l['group_public_moderated'] = "这是一个公共的用户组, 任何人均可以加入. 但所有的加入请求必须经过组长审核批准。";
$l['group_public_not_moderated'] = "这是一个公共的用户组, 任何人均可以自由地加入, 不用等候审核。";
$l['group_public_invite'] = "这是一个公共的用户组, 但需要组长邀请才能加入。";
$l['group_private'] = "这是一个私有的用户组. 只有组长可以添加成员。";
$l['group_default'] = "这是一个核心用户组";
$l['group_leaders'] = "组管理员";
$l['search_user'] = "查找成员";
$l['no_users_selected'] = "抱歉，似乎没有用户要删除。<br />请返回并选择该组要删除的用户。";

$l['error_alreadyingroup'] = "该用户已经是这个组的成员了。";
$l['error_alreadyinvited'] = "已向该用户发出过邀请";

$l['user_invited'] = "该用户已被邀请加入该用户组.";
$l['invite_pm_subject'] = "您被邀请加入 \"{1}\"";
$l['invite_pm_message'] = "你收到了加入用户组 \"{1}\"的邀请.
同意加入，请进入你的 [url={2}/usercp.php?action=usergroups]用户组成员[/url] 页面并点击 '同意邀请'.
此邀请不会过期。";
$l['invite_pm_message_expires'] = "你收到了加入用户组 \"{1}\"的邀请.
同意加入，请进入你的 [url={2}/usercp.php?action=usergroups]用户组成员[/url] 页面并点击 '同意邀请'.
此邀请将在 {3} 天后过期。";

