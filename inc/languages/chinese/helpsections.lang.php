<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

// Help Section 1
$l['s1_name'] = "用户(会员)";
$l['s1_desc'] = "会员用户的基本使用说明。";

// Help Section 2
$l['s2_name'] = "发帖";
$l['s2_desc'] = "发表、回复以及论坛的基础运用。";
