<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['redirect_threadrated'] = "非常感谢, 您已成功地给该帖评分, 您将返回该主题。";

$l['error_invalidrating'] = "您为该主题选择了一个无效的评分, 请返回重试。";
$l['error_alreadyratedthread'] = "抱歉, 您已经给该主题评过分。";
$l['error_cannotrateownthread'] = "对不起，您无法对自己的主题评分";

$l['rating_votes_average'] = "{1} 次(票) - 平均星级: {2}/5";
$l['one_star'] = "一星";
$l['two_stars'] = "两星";
$l['three_stars'] = "三星";
$l['four_stars'] = "四星";
$l['five_stars'] = "五星";
$l['rating_added'] = "已加入您的评分!";
