<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['nav_announcements'] = "论坛公告";
$l['announcements'] = "公告";
$l['forum_announcement'] = "论坛公告: {1}";
$l['error_invalidannouncement'] = "指定的公告无效。";

$l['announcement_edit'] = "编辑公告";
$l['announcement_qdelete'] = "删除公告";
$l['announcement_quickdelete_confirm'] = "确定删除该公告?";

