<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['archive_fullversion'] = "完整版:";
$l['archive_replies'] = "回复数";
$l['archive_reply'] = "回复";
$l['archive_pages'] = "页:";
$l['archive_note'] = "你目前正在浏览的是简化版.  <a href=\"{1}\">请点击这里浏览完整版</a>";
$l['archive_nopermission'] = "抱歉,你没有权限访问此资源。";
$l['error_nothreads'] = "这个讨论区目前没有主题";
$l['error_unapproved_thread'] = "此主题未通过审核。<a href=\"{1}\">请点击这里浏览</a>该主题的全部内容";
$l['archive_not_found'] = "服务器上找不到该页面。";
$l['error_mustlogin'] = "此版块需要登录后浏览";