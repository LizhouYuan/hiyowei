<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['postdata_invalid_user_id'] = '这个使用者代号不存在, 请提供一个有效的用户代号.';
$l['postdata_firstpost_no_subject'] = '这个主题还没有标题, 请输入标题.';
$l['postdata_missing_subject'] = '缺少标题, 请输入.';
$l['postdata_missing_message'] = '缺少帖子内容, 请输入.';
$l['postdata_message_too_long'] = '帖子内容太长了, 请输入少于 {1} 个字符的内容(当前 {2})';
$l['postdata_message_too_short'] = '帖子内容太短了, 请输入至少 {1} 个字符';
$l['postdata_subject_too_long'] = '标题太长了， 请输入少于 85个字符的标题 (当前 {1})';
$l['postdata_banned_username'] = '您输入的用户名无法注册, 请输入其它用户名.';
$l['postdata_bad_characters_username'] = '您输入的用户名包含了无效字符, 请其它用户名.';
$l['postdata_invalid_username_length'] = '您输入的用户名长度不正确, 请控制在 {1} 到 {2} 个字符.';
$l['postdata_post_flooding'] = '距离您的上一篇帖子时间太短了, 请再等待 {1} 秒.';
$l['postdata_post_flooding_one_second'] = '距离您的上一篇帖子时间太短了, 请再等待 1 秒.';
$l['postdata_too_many_images'] = '您输入的帖子内容包含 {1} 张图片, 然而每篇帖子只允许 {2} 张图片. 请减少图片数量以符合限制.';
$l['postdata_too_many_videos'] = '您输入的帖子内容包含 {1} 个视频, 然而每篇帖子只允许 {2} 个视频。请减少视频数量以符合限制.';
$l['postdata_invalid_prefix'] = '选择的前缀无效，请选择一个有效的前缀';
$l['postdata_require_prefix'] = '本论坛要求选择一个主题前缀，请选择前缀。';

$l['thread_closed'] = "主题已关闭";
$l['thread_opened'] = "主题开放中";
$l['thread_stuck'] = "主题已置顶";
$l['thread_unstuck'] = "主题未置顶";

