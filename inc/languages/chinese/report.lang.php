<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['report_reason'] = "举报理由:";
$l['report_to_mod'] = "向管理员举报此贴";
$l['close_window'] = "关闭窗口";

// Content types
$l['report_content'] = "举报内容";
$l['report_reason_post'] = "举报帖子";
$l['report_reason_profile'] = "举报个人资料";
$l['report_reason_reputation'] = "举报声望";

// Content reasons
$l['report_reason_bad'] = "不良内容";
$l['report_reason_spam'] = "垃圾内容";
$l['report_reason_rules'] = "违反版规";
$l['report_reason_wrong'] = "发错版块";
$l['report_reason_other'] = "其它";

$l['success_report_voted'] = "谢谢您的举报.<br />我们将尽快处理您的举报";

$l['error_report_length'] = "请提供详细的举报理由.";
$l['error_invalid_report'] = "内容不存在或无法被举报.";
$l['error_report_duplicate'] = "该内容已被其它成员举报.<br />您也可以在下面举报该内容.";
$l['report_reason_other_description'] = "如果是'其它' 请填写举报理由.";
