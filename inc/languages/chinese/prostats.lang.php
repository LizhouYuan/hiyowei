<?php
/**
 * ProStats Plugin - English language
 */

$l['prostats_prostats'] = '今日头条';
$l['prostats_reload'] = '刷新';

$l['prostats_username'] = '用户名';
$l['prostats_posts'] = '贴子';
$l['prostats_datetime'] = '日期';
$l['prostats_forum'] = '论坛';

$l['prostats_latest_posts'] = '最新发布';
$l['prostats_most_replies'] = '回帖热榜';
$l['prostats_most_reputations'] = '名人堂';
$l['prostats_most_thanks'] = '点赞榜';
$l['prostats_most_views'] = '十大热门';
$l['prostats_newest_members'] = '新同学';
$l['prostats_top_downloads'] = '下载排行榜';
$l['prostats_top_posters'] = '灌水达人榜';
$l['prostats_top_thread_posters'] = '话题达人榜';
$l['prostats_top_topreferrers'] = '嗨哟喂之友';

$l['prostats_topic'] = '话题';
$l['prostats_author'] = '作者';
$l['prostats_last_sender'] = '最新回复';
$l['prostats_replies'] = '回复';

$l['prostats_views'] = '浏览';
$l['prostats_subject'] = '标题';

$l['prostats_err_thxplugin'] = 'Please install the <a href="http://prostats.wordpress.com/support/">Thanks</a> plugin.';
$l['prostats_err_undefind'] = 'Undefind';

$l['prostats_disabled'] = 'You cannot use the prostats functionality as it has been disabled by the Administrator.';
