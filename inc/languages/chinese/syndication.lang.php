<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['all_forums'] = "所有版块";
$l['forum'] = "版块：";
$l['posted_by'] = "发表:";
$l['on'] = "开启";
$l['portal'] = "个人网站";

