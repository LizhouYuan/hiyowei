<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['warnings_reached_max_warnings_day'] = '您不能发出警告了, 因为您已经达到了每日可以发送的警告数量上限.<br /><br />您每天可以发出的警告最多次数为 {1}。"';
$l['warnings_error_invalid_user'] = "选择的用户不存在。";
$l['warnings_error_invalid_post'] = "选择的帖子不存在。";
$l['warnings_error_cannot_warn_self'] = "您不能给自己增加警告等级。";
$l['warnings_error_user_reached_max_warning'] = "这个用户不能再被警告了, 因为他已经达到了警告等级的最大值。";
$l['warnings_error_no_note'] = "您没有针对这条警告输入任何管理备注。";
$l['warnings_error_invalid_type'] = "您选择了一个无效的警告类型。";
$l['warnings_error_cant_custom_warn'] = "您没有权限对用户发出自定义警告。";
$l['warnings_error_no_custom_reason'] = "您没有针对您的自定义警告输入原因。";
$l['warnings_error_invalid_custom_points'] = "您没有输入一个有效要增加的警告点数. 您需要输入一个大于 0 并且小于或等于 {1} 的数字。";
$l['warnings_error_invalid_expires_period'] = "您输入了一个无效的截止类型";
