<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['nav_editpost'] = "编辑帖子";
$l['edit_post'] = "编辑这个帖子";
$l['delete_post'] = "删除帖子";
$l['delete_q'] = "删除？";
$l['delete_1'] = "若要删除这个帖子, 请选中左边的复选框然后点击右边的按钮。";
$l['delete_2'] = "<b>注意:</b> 如果这个帖子是主题的第一帖(楼顶), 那将导致删除整个主题。";
$l['subject'] = "标题";
$l['your_message'] = "帖子内容";
$l['post_options'] = "帖子选项:";
$l['editreason'] = "编辑理由：";
$l['options_sig'] = "<strong>加入签名:</strong> 在帖子中包含显示您的签名. (仅注册用户可用)";
$l['options_emailnotify'] = "<strong>Email 通知:</strong> 只要有新回帖时发送 Email 通知我. (仅注册用户可用)";
$l['options_disablesmilies'] = "<strong>禁用表情:</strong> 禁止在显示帖子内容里解析表情图片。";
$l['preview_post'] = "预览";
$l['update_post'] = "保存更改";
$l['poll'] = "投票:";
$l['poll_desc'] = "您可以在这个主题中附带一个投票(可选的)。";
$l['poll_check'] = "我要附带发起投票";
$l['num_options'] = "选项个数:";
$l['max_options'] = "(最多: {1})";
$l['delete_now'] = "立即删除";
$l['edit_time_limit'] = "很抱歉, 您不能编辑修改您的帖子. 因为管理员设定必须在发布 {1} 分钟后才能被编辑。";
$l['no_prefix'] = "没有前缀";

$l['redirect_nodelete'] = "帖子没有被删除, 因为您没有选中(打勾) \"删除\" 这个复选框。";
$l['redirect_norestore'] = "帖子没有被恢复, 因为您没有选中 \"恢复\" 这个复选框。";
$l['redirect_postedited'] = "非常感谢, 这个帖子已经修改完成.<br />";
$l['redirect_postedited_redirect'] = "您现在将转入访问这个主题。";
$l['redirect_postedited_poll'] = "非常感谢, 这个帖子已经修改完成. <br />由于您选择附带投票, 您将转入建立投票的页面。";
$l['error_invalidpost'] = "抱歉, 似乎您点击了一个无效的地址. 请确定您要编辑的帖是否存在并重试。";
$l['redirect_threaddeleted'] = "非常感谢, 该主题已经成功删除.<br />您现在将转入访问论坛版面。";
$l['redirect_postdeleted'] = "非常感谢, 该帖子已经成功删除.<br />您现在将转入访问所在主题。";
$l['redirect_threadrestored'] = "非常感谢，主题已恢复。<br />您现在将返回论坛。";
$l['redirect_postrestored'] = "非常感谢，该帖已恢复。<br />您现在将返回主题。";
$l['redirect_threadclosed'] = "您无法编辑这个主题里的帖子, 因为它已经被管理员或版主锁定了。";
$l['redirect_post_moderation'] = "管理员设定了只有管理员或版主才能进行帖子编辑. 你现在将转入访问这个主题。";
$l['redirect_thread_moderation'] = "管理员设定了只有管理员或版主才能进行主题编辑. 你现在将转入访问论坛版面。";
$l['error_already_delete'] = "对不起，该帖已被删除。";

$l['thread_deleted'] = "永久删除此主题";
$l['post_deleted'] = "永久删除此回复";
$l['thread_soft_deleted'] = "回收主题";
$l['post_soft_deleted'] = "常规删除的回复";
$l['thread_restored'] = "已恢复的主题";
$l['post_restored'] = "已恢复的回帖";

$l['error_already_deleted'] = '选择的回帖已被删除';
