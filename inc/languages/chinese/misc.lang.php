<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['nav_helpdocs'] = "帮助文档";
$l['nav_smilies'] = "表情列表";
$l['nav_syndication'] = "最近的主题聚合 (RSS)";

$l['aol_im'] = "AOL IM";
$l['skype'] = "Skype";
$l['yahoo_im'] = "Yahoo IM";
$l['skype_center'] = "Skype 中心";
$l['skype_status'] = "Skype 状态";
$l['chat_on_skype'] = "用Skype与{1}聊天";
$l['call_on_skype'] = "用Skype呼叫{1}";
$l['yahoo_center'] = "Yahoo! 中心";
$l['send_y_message'] = "给 {1} 发送 Yahoo! 讯息。";
$l['view_y_profile'] = "查看 {1} 的 Yahoo! 档案。";
$l['aim_center'] = "AOL IM 中心";

$l['download_aim'] = "下载 AIM";
$l['aim_remote'] = "AIM Remote";
$l['send_me_instant'] = "给我发送一条即时讯息";
$l['add_me_buddy_list'] = "把我加到您的好友名单";
$l['add_remote_to_page'] = "Add Remote to Your Page";
$l['download_aol_im'] = "下载 AOL 即时通讯";

$l['buddy_list'] = "好友列表";
$l['online'] = "在线";
$l['online_none'] = "<em>无在线好友</em>";
$l['offline'] = "离线";
$l['offline_none'] = "<em>Y无离线好友</em>";
$l['delete_buddy'] = "X";
$l['pm_buddy'] = "发送PM";
$l['last_active'] = "<strong>最近活动:</strong> {1}";
$l['close'] = "关闭";
$l['no_buddies'] = "<em>您的好友名单当前为空。使用用户控制面板或者访问一个用户的个人资料来添加用户到您的好友名单中。</em>";

$l['help_docs'] = "帮助文档";

$l['search_help_documents'] = "搜索帮助文档";
$l['search_by_name'] = "按名称搜索";
$l['search_by_document'] = "按文档搜索";
$l['enter_keywords'] = "输入关键字";
$l['search'] = "搜索";
$l['redirect_searchresults'] = "感谢您搜索，您的搜索内容已经提交，现在将为您呈现搜索结果。";
$l['search_results'] = "搜索结果";
$l['help_doc_results'] = "帮助文档结果";
$l['document'] = "文档";
$l['error_nosearchresults'] = "对不起，没有您想要的结果，请重新搜索。";
$l['no_help_results'] = "对不起，没有您想要的结果。";
$l['error_helpsearchdisabled'] = "搜索帮助文档的功能已被管理员关闭";

$l['smilies_listing'] = "表情列表";
$l['name'] = "名称";
$l['abbreviation'] = "缩写";
$l['click_to_add'] = "点击一个表情插入到您的信息中";
$l['close_window'] = "关闭窗口";

$l['who_posted'] = "谁发的?";
$l['total_posts'] = "总帖数:";
$l['user'] = "用户";
$l['num_posts'] = "帖数";

$l['forum_rules'] = "{1} - 规则";

$l['error_invalid_limit'] = "输入的限制数无效，请指定一个有效的限制数。";

$l['syndication'] = "最新主题同步";
$l['syndication_generated_url'] = "您生成的同步网址:";
$l['syndication_note'] = "下面您可以生成RSS同步feed链接，所有的版面均可生成，你将得到一个可以在RSS阅读器中使用的链接<i><a href=\"http://en.wikipedia.org/wiki/RSS\" target=\"_blank\">什么是RSS?</a></i>";
$l['syndication_forum'] = "同步的版块:";
$l['syndication_forum_desc'] = "请从右边选择一个版块。按住CTRL键选择多个版块。";
$l['syndication_version'] = "Feed 版本:";
$l['syndication_version_desc'] = "请选择你希望生成的feed版本";
$l['syndication_version_atom1'] = "Atom 1.0";
$l['syndication_version_rss2'] = "RSS 2.00 (默认)";
$l['syndication_generate'] = "生成同步网址";
$l['syndication_limit'] = "限制数:";
$l['syndication_limit_desc'] = "一次下载主题数量。建议不要超过50。";
$l['syndication_threads_time'] = "在同一时间主题";
$l['syndicate_all_forums'] = "同步所有版块";

$l['redirect_markforumread'] = "选定的版块已经被标记为已读。";
$l['redirect_markforumsread'] = "所有版块已经被标记为已读。";
$l['redirect_forumpasscleared'] = "该板块的密码已经被清除。";
$l['redirect_cookiescleared'] = "所有cookies已经清除。";

$l['error_invalidimtype'] = "该用户没有在他的个人资料里设定这种类型的即时通讯帐号。";
$l['error_invalidhelpdoc'] = "指定的帮助文档貌似不存在。";
$l['error_invalidkey'] = "您无法清除cookie。这可能是因为恶意Javascript正试图自动清除您的cookie。如果您打算清除您的cookie，请阅读 \"MyBB中Cookie的使用\"帮助文档。";

$l['dst_settings_updated'] = "您的夏令时设置已经自动调整。<br /><br />现在将跳转到论坛首页。";
