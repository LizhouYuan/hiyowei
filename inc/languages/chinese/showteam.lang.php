<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['nav_showteam'] = "管理团队";
$l['forum_team'] = "管理团队";
$l['moderators'] = "版主";
$l['username'] = "用户名";
$l['lastvisit'] = "最近访问";
$l['email'] = "Email";
$l['pm'] = "私人短信(PM)";
$l['mod_forums'] = "版面";
$l['online'] = "在线";
$l['offline'] = "离线";

$l['group_leaders'] = "组长";
$l['group_members'] = "成员";

$l['no_members'] = "这个组里没有成员。";

$l['error_noteamstoshow'] = "没有论坛工作人员可以显示。";
