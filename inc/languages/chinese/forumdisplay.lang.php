<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['post_thread'] = "发表新主题";
$l['moderated_by'] = "版主:";
$l['nothreads'] = "抱歉, 这个版块里目前没有位于指定日期与时间限制内的主题。";
$l['search_forum'] = "搜索这个版面:";
$l['thread'] = "主题";
$l['author'] = "作者";
$l['replies'] = "回复数";
$l['views'] = "快速查看";
$l['lastpost'] = "最近发表";
$l['rating'] = "评分";
$l['prefix'] = "前缀：";
$l['prefix_all'] = "前缀：所有前缀";
$l['markforum_read'] = "标记此版面为已读";
$l['subscribe_forum'] = "订阅这个版面";
$l['unsubscribe_forum'] = "取消订阅这个版面";
$l['clear_stored_password'] = "清除已存入的论坛密码";
$l['sort_by_subject'] = "排序: 主题";
$l['sort_by_lastpost'] = "排序: 最近发表";
$l['sort_by_starter'] = "排序: 作者";
$l['sort_by_started'] = "排序: 发布时间";
$l['sort_by_rating'] = "排序: 评分";
$l['sort_by_replies'] = "排序: 回复数";
$l['sort_by_views'] = "排序: 人气";
$l['sort_order_asc'] = "升序排列";
$l['sort_order_desc'] = "降序排列";
$l['datelimit_1day'] = "从: 昨天以来";
$l['datelimit_5days'] = "从: 5 天以来";
$l['datelimit_10days'] = "从: 10 天以来";
$l['datelimit_20days'] = "从: 20 天以来";
$l['datelimit_50days'] = "从: 50 天以来";
$l['datelimit_75days'] = "从: 75 天以来";
$l['datelimit_100days'] = "从: 100 天以来";
$l['datelimit_lastyear'] = "从: 1 年以来";
$l['datelimit_beginning'] = "从: 开始以来";
$l['new_thread'] = "新帖";
$l['new_hot_thread'] = "热门主题 (新)";
$l['posts_by_you'] = "包含你的回帖";
$l['no_new_thread'] = "无新帖";
$l['hot_thread'] = "热门主题 (无新贴)";
$l['locked_thread'] = "锁定的主题";
$l['goto_first_unread'] = "转到首个未读帖子";
$l['pages'] = "页:";
$l['pages_last'] = "最后一页";
$l['users_browsing_forum'] = "正在浏览本论坛的用户：";
$l['users_browsing_forum_guests'] = "{1} 个游客";
$l['users_browsing_forum_invis'] = "{1} 个隐身的用户";
$l['delayed_moderation'] = "延迟的处理";
$l['inline_thread_moderation'] = "主题处理:";
$l['close_threads'] = "关闭主题";
$l['open_threads'] = "打开主题";
$l['stick_threads'] = "合并主题";
$l['unstick_threads'] = "拆分主题";
$l['soft_delete_threads'] = "回收主题";
$l['restore_threads'] = "恢复主题";
$l['delete_threads'] = "永久删除主题";
$l['move_threads'] = "移动主题";
$l['approve_threads'] = "审核主题";
$l['unapprove_threads'] = "未审核主题";
$l['inline_go'] = "Go";
$l['clear'] = "清除";
$l['sub_forums_in']  = "'{1}' 的分论坛";
$l['forum_rules'] = "{1} - 规则";
$l['subforums'] = "<strong>子版块:</strong>";
$l['asc'] = "升序";
$l['desc'] = "降序";
$l['forum_announcements'] = "论坛公告";
$l['sticky_threads'] = "重要主题";
$l['normal_threads'] = "普通主题";
$l['icon_dot'] = "你回复过的"; // The spaces for the icon labels are strategically placed so that there should be no extra space at the beginning or end of the resulting label and that spaces separate each 'status' ;)
$l['icon_no_new'] = "无新回复";
$l['icon_new'] = "有新回复";
$l['icon_hot'] = "热门主题";
$l['icon_lock'] = "已关闭";
$l['attachment_count'] = "此帖包含 1 个附件";
$l['attachment_count_multiple'] = "此帖包含 {1} 个附件";
$l['rss_discovery_forum'] = "{1} 的最新主题";
$l['forum_unapproved_posts_count'] = "论坛目前有 {1} 个未审核的帖子。";
$l['forum_unapproved_post_count'] = "论坛目前有 1 个帖子。";
$l['forum_unapproved_threads_count'] = "本版目前有 {1} 个未审核的主题。";
$l['forum_unapproved_thread_count'] = "论坛目前有 1 个主题。";
$l['thread_unapproved_posts_count'] = "这个主题中有 {1} 个未审核的帖子。";
$l['thread_unapproved_post_count'] = "这个主题中有 1 个未审核的帖子。";
$l['page_selected'] = "选择了当前页面的所有主题共 <strong>{1}</strong> 个。";
$l['all_selected'] = "选择了当前版块的所有主题共 <strong>{1}</strong> 个。";
$l['select_all'] = "选择当前板块的所有主题，共 <strong>{1}</strong> 个。";
$l['clear_selection'] = "清除选择。";

$l['error_containsnoforums'] = "抱歉, 您正在浏览的版面并未包含任何分论坛。";

$l['inline_edit_description'] = '(点击并按住可以编辑)';

