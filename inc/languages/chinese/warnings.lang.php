<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['nav_profile'] = "{1} 的个人资料";
$l['nav_warning_log'] = "警告日志";
$l['nav_add_warning'] = "警告用户";
$l['nav_view_warning'] = "警告细节";

$l['warning_for_post'] = ".. 帖子:";
$l['already_expired'] = "过期的";
$l['details_username'] = "用户名";
$l['warning_active'] = "Active";
$l['warning_revoked'] = "撤销的";
$l['warning_log'] = "警告日志";
$l['warning'] = "警告";
$l['issued_by'] = "颁布用户";
$l['date_issued'] = "日期颁布";
$l['expiry_date'] = "过期";
$l['active_warnings'] = "现行警告(起作用的)";
$l['expired_warnings'] = "已过期警告";
$l['warning_points'] = "({1} 点)";
$l['no_warnings'] = "这个用户没有收到任何警告, 或已被全部删除。";
$l['warn_user'] = "警告用户";
$l['post'] = "帖子：";
$l['warning_note'] = "管理备注:";
$l['details_warning_note'] = "Administrative Notes";
$l['warning_type'] = "警告类型:";
$l['custom'] = "自定义原因";
$l['reason'] = "理由：";
$l['points'] = "点数:";
$l['details_reason'] = "理由";
$l['warn_user_desc'] = "如果用户违反了一个或更多的规则, 您可以在这里增加用户的警告等级。";
$l['send_pm'] = "通知用户:";
$l['send_user_warning_pm'] = "给这个用户发送一条私人短信通知他这个警告。";
$l['send_pm_subject'] = "标题:";
$l['warning_pm_subject'] = "您收到了一条警告。";
$l['send_pm_message'] = "内容:";
$l['warning_pm_message'] = "亲爱的 {1}

您收到了一条来自{2}管理员的警告.
--

--";
$l['send_pm_options'] = "短信选项:";
$l['send_pm_options_anonymous'] = "<strong>匿名PM</strong>: 以匿名用户发送此短信息.";
$l['expiration_never'] = "永久";
$l['expiration_hours'] = "小时";
$l['expiration_days'] = "天";
$l['expiration_weeks'] = "星期";
$l['expiration_months'] = "个月";
$l['redirect_warned_banned'] = "<br /><br />该用户同时被移入了 {1} 组 {2}.";
$l['redirect_warned_suspended'] = "<br /><br />此用户发帖权限已被封停 {1}.";
$l['redirect_warned_moderate'] = "<br /><br />此用户的所有发帖将被审核 {1}.";
$l['redirect_warned_pmerror'] = "<br /><br />请注意短消息没有被发送.";
$l['redirect_warned'] = "{1} 的警告等级已经被增加到 {2}%.{3}<br /><br />您现在将转入您原来的位置。";
$l['error_warning_system_disabled'] = "您不能使用警告系统, 因为论坛管理员已经禁用这个功能。";
$l['error_cant_warn_group'] = "您没有权限警告这个组的用户。";
$l['error_invalid_user'] = "选择的用户不存在。";
$l['details'] = "细节";
$l['view'] = "快速查看";
$l['current_warning_level'] = "当前警告等级: <strong>{1}%</strong> ({2}/{3})";
$l['warning_details'] = "警告细节";
$l['revoke_warning'] = "撤销该警告";
$l['revoke_warning_desc'] = "撤销这项警告，请输入下面的一个原因。这将不会删除任何禁止或暂停实行这一警告。";
$l['warning_is_revoked'] = "这条警告已撤销";
$l['revoked_by'] = "撤销者";
$l['date_revoked'] = "撤销日期";
$l['warning_already_revoked'] = "警告已经撤销过了。";
$l['no_revoke_reason'] = "没有输入撤销的原因。";
$l['redirect_warning_revoked'] = "这条警告已经成功撤销, 用户的警告等级也随之下降.<br /><br />现在将返回到那个警告。";
$l['result'] = "结果:";
$l['result_banned'] = "用户将被移入一个封禁组({1}) {2}";
$l['result_suspended'] = "发表权限将被暂停 {1}";
$l['result_moderated'] = "发帖将被审核 {1}";
$l['result_period'] = "{1} {2}";
$l['result_period_perm'] = "永久";
$l['hour_or_hours'] = "小时";
$l['day_or_days'] = "天";
$l['week_or_weeks'] = "周";
$l['month_or_months'] = "月";
$l['expires'] = "持续时间:";
$l['new_warning_level'] = "新警告等级:";
$l['error_cant_warn_user'] = "您没有权限对这个用户发出警告。";
$l['existing_post_warnings'] = "这个帖子现有的警告";
