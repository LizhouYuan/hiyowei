<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['redirect_width'] = "50%";
$l['lastvisit_never'] = "从未";
$l['lastvisit_hidden'] = "(隐藏)";

$l['search_button'] = '搜索';
$l['toplinks_memberlist'] = "会员列表";
$l['toplinks_search'] = "搜索";
$l['toplinks_calendar'] = "日历";
$l['toplinks_help'] = "帮助";
$l['toplinks_portal'] = "门户";
$l['toplinks_misc'] = "更多";
$l['bottomlinks_contactus'] = "联系我们";
$l['bottomlinks_returntop'] = "返回顶部";
$l['bottomlinks_syndication'] = "RSS 聚合";
$l['bottomlinks_litemode'] = "精简模式";
$l['bottomlinks_markread'] = "标记所有论坛已读";
$l['title'] = "嗨哟喂!";

$l['welcome_usercp'] = "个人主页";
$l['welcome_modcp'] = "版主面板";
$l['welcome_admin'] = "系统后台";
$l['welcome_logout'] = "退出";
$l['welcome_login'] = "登录";
$l['welcome_register'] = "注册";
$l['welcome_hint'] = "留学生的万能社区";
$l['welcome_open_buddy_list'] = "朋友圈";
$l['welcome_newposts'] = "看新帖";
$l['welcome_todaysposts'] = "今日发布";
$l['welcome_pms'] = "私信";
$l['welcome_pms_usage'] = "(未读 {1}, 总共 {2})";
$l['welcome_back'] = " 欢迎回来!";
$l['welcome_guest'] = "欢迎来到嗨哟喂! ";
$l['welcome_current_time'] = "<strong>当前时间:</strong> {1}";

$l['moved_prefix'] = "移动:";
$l['poll_prefix'] = "投票:";

$l['forumbit_announcements'] = "公告";
$l['forumbit_stickies'] = "重要主题";
$l['forumbit_forum'] = "分论坛";
$l['forumbit_threads'] = "话题";
$l['forumbit_posts'] = "帖子";
$l['forumbit_lastpost'] = "最新发表";
$l['forumbit_moderated_by'] = "版主:";
$l['new_posts'] = "有新帖子的论坛";
$l['no_new_posts'] = "无新帖子的论坛";
$l['click_mark_read'] = "点击标记该板块为已读";
$l['forum_locked'] = "已锁定的论坛";
$l['forum_redirect'] = "跳转论坛";
$l['lastpost_never'] = "无";
$l['viewing_one'] = " (1 用户正在浏览)";
$l['viewing_multiple'] = " ({1} 位用户正在浏览)";
$l['by'] = "by";
$l['more_subforums'] = "还有 {1} 个。";

$l['password_required'] = "需要密码";
$l['forum_password_note'] = "系统管理员设定进入这个论坛需要密码。";
$l['enter_password_below'] = "请在下面输入密码:";
$l['verify_forum_password'] = "核对论坛密码";
$l['wrong_forum_password'] = "您输入的密码不正确. 请重试。";

$l['reset_button'] = "重置";
$l['username'] = "用户名:";
$l['username1'] = "Email:";
$l['username2'] = "用户名/Email:";
$l['password'] = "密码:";
$l['login_username'] = "用户名:";
$l['login_username1'] = "Email:";
$l['login_username2'] = "用户名/Email:";
$l['login_password'] = "密码:";
$l['lost_password'] = "忘记密码?";
$l['remember_me'] = "记住我";
$l['remember_me_desc'] = "如果勾选，您的登录细节将被记在这台计算机上，否则，一旦关闭浏览器就将退出。";

$l['month_1'] = "一月";
$l['month_2'] = "二月";
$l['month_3'] = "三月";
$l['month_4'] = "四月";
$l['month_5'] = "五月";
$l['month_6'] = "六月";
$l['month_7'] = "七月";
$l['month_8'] = "八月";
$l['month_9'] = "九月";
$l['month_10'] = "十月";
$l['month_11'] = "十一月";
$l['month_12'] = "十二月";

$l['sunday'] = "星期天";
$l['monday'] = "星期一";
$l['tuesday'] = "星期二";
$l['wednesday'] = "星期三";
$l['thursday'] = "星期四";
$l['friday'] = "星期五";
$l['saturday'] = "星期六";
$l['short_monday'] = "M";
$l['short_tuesday'] = "T";
$l['short_wednesday'] = "W";
$l['short_thursday'] = "T";
$l['short_friday'] = "F";
$l['short_saturday'] = "S";
$l['short_sunday'] = "S";

$l['yes'] = "是";
$l['no'] = "否";

$l['and'] = "和";
$l['date'] = "日期";

$l['nobody'] = "无人";

$l['attachments'] = "附件";
$l['attachments_desc'] = "您可以附加一个或多个附件到帖子中. 请在右边选取文件并点击 '增加附件' 以上载。";
$l['remove_attachment'] = "删除";
$l['approve_attachment'] = "已审核";
$l['unapprove_attachment'] = "未审核";
$l['insert_attachment_post'] = "插入帖子";
$l['new_attachment'] = "新增附件:";
$l['add_attachment'] = "上传附件";
$l['update_attachment'] = "更新附件";
$l['post_preview'] = "预览";
$l['change_user'] = "更换用户";
$l['post_icon'] = "帖子图标:";
$l['no_post_icon'] = "无图标";
$l['thread_subscription_method'] = "主题订阅:";
$l['thread_subscription_method_desc'] = "请指定针对该主题订阅的通知类型. (仅已注册用户)";
$l['no_subscribe'] = "请不要订阅该主题";
$l['no_subscribe_notification'] = "订阅但不接收任何新回复提醒";
$l['instant_email_subscribe'] = "订阅并接收新回复的邮件提醒";
$l['instant_pm_subscribe'] = "订阅并接收新回复的PM提醒";

$l['today_rel'] = "<span title=\"{1}\">今天</span>";
$l['yesterday_rel'] = "<span title=\"{1}\">昨天</span>";
$l['today'] = "今天";
$l['yesterday'] = "昨天";
$l['error'] = "论坛信息";

$l['multipage_pages'] = "页 ({1}):";
$l['multipage_last'] = "末页";
$l['multipage_first'] = "首页";
$l['multipage_next'] = "下一页";
$l['multipage_previous'] = "上一页";
$l['multipage_link_start'] = " ...";
$l['multipage_link_end'] = "... ";
$l['multipage_jump'] = "跳转到";

$l['editor_bold'] = "粗体";
$l['editor_italic'] = "斜体";
$l['editor_underline'] = "下划线";
$l['editor_strikethrough'] = "删除线";
$l['editor_subscript'] = "下标";
$l['editor_superscript'] = "上标";
$l['editor_alignleft'] = "左对齐";
$l['editor_center'] = "居中对齐";
$l['editor_alignright'] = "右对齐";
$l['editor_justify'] = "对齐文本";
$l['editor_fontname'] = "字体";
$l['editor_fontsize'] = "字体大小";
$l['editor_fontcolor'] = "字体颜色";
$l['editor_removeformatting'] = "移除格式";
$l['editor_cut'] = "剪切";
$l['editor_copy'] = "复制";
$l['editor_paste'] = "粘贴";
$l['editor_cutnosupport'] = "您的浏览器不支持剪切命令，请使用快捷键 Ctrl/Cmd-X";
$l['editor_copynosupport'] = "您的浏览器不支持复制命令，请使用快捷键 Ctrl/Cmd-C";
$l['editor_pastenosupport'] = "您的浏览器不支持粘贴命令，请使用快捷键 Ctrl/Cmd-V";
$l['editor_pasteentertext'] = "请在下面文本框中粘贴内容:";
$l['editor_pastetext'] = "粘贴文本";
$l['editor_numlist'] = "数字有序列表";
$l['editor_bullist'] = "无序圆点列表";
$l['editor_undo'] = "撤销";
$l['editor_redo'] = "恢复";
$l['editor_rows'] = "行:";
$l['editor_cols'] = "列:";
$l['editor_inserttable'] = "插入表格";
$l['editor_inserthr'] = "插入水平线";
$l['editor_code'] = "插入格式化代码";
$l['editor_php'] = "插入PHP代码";
$l['editor_width'] = "宽度 (可选):";
$l['editor_height'] = "高度 (可选):";
$l['editor_insertimg'] = "插入图片";
$l['editor_email'] = "E-mail:";
$l['editor_insertemail'] = "插入Email";
$l['editor_url'] = "网址:";
$l['editor_insertlink'] = "插入链接";
$l['editor_unlink'] = "取消链接";
$l['editor_more'] = "更多";
$l['editor_insertemoticon'] = "插入表情";
$l['editor_videourl'] = "视频网址:";
$l['editor_videotype'] = "视频类型:";
$l['editor_insert'] = "插入";
$l['editor_insertyoutubevideo'] = "插入YouTube视频";
$l['editor_currentdate'] = "插入当前日期";
$l['editor_currenttime'] = "插入当前时间";
$l['editor_print'] = "打印";
$l['editor_viewsource'] = "查看源代码";
$l['editor_description'] = "描述 (可选):";
$l['editor_enterimgurl'] = "输入图片网址:";
$l['editor_enteremail'] = "输入E-mail地址:";
$l['editor_enterdisplayedtext'] = "输入要显示的文本:";
$l['editor_enterurl'] = "输入网址:";
$l['editor_enteryoutubeurl'] = "输入YouTube视频网址或ID:";
$l['editor_insertquote'] = "插入引用";
$l['editor_invalidyoutube'] = "无效的YouTube视频";
$l['editor_dailymotion'] = "Dailymotion";
$l['editor_metacafe'] = "MetaCafe";
$l['editor_veoh'] = "Veoh";
$l['editor_vimeo'] = "Vimeo";
$l['editor_youtube'] = "Youtube";
$l['editor_facebook'] = "Facebook";
$l['editor_liveleak'] = "LiveLeak";
$l['editor_insertvideo'] = "插入视频";
$l['editor_maximize'] = "最大化";

$l['quote'] = "引用:";
$l['wrote'] = "提到:";
$l['code'] = "代码:";
$l['php_code'] = "PHP 代码:";
$l['posted_image'] = "[图: {1}]";
$l['posted_video'] = "[视频: {1}]";
$l['linkback'] = "原帖";

$l['at'] = "在";
$l['na'] = "N/A";
$l['guest'] = "游客";
$l['unknown'] = "未知";
$l['never'] = "从不";
$l['postbit_posts'] = "帖子:";
$l['postbit_threads'] = "主题:";
$l['postbit_group'] = "群组:";
$l['postbit_joined'] = "加入时间:";
$l['postbit_status'] = "状态:";
$l['postbit_attachments'] = "附件";
$l['postbit_attachment_size'] = "大小:";
$l['postbit_attachment_downloads'] = "下载:";
$l['postbit_attachments_images'] = "图片";
$l['postbit_attachments_thumbnails'] = "缩略图";
$l['postbit_unapproved_attachments'] = "{1} 个未审核附件.";
$l['postbit_unapproved_attachment'] = "1 个未审核附件.";
$l['postbit_status_online'] = "在线";
$l['postbit_status_offline'] = "离线";
$l['postbit_status_away'] = "离开";
$l['postbit_edited'] = "这个帖子最后修改于: {1} by";
$l['postbit_editreason'] = "编辑原因";
$l['postbit_ipaddress'] = "IP 地址:";
$l['postbit_ipaddress_logged'] = "已记录";
$l['postbit_post'] = "帖子:";
$l['postbit_reputation'] = "声望:";
$l['postbit_reputation_add'] = "为该用户添加声望";
$l['postbit_website'] = "访问该用户的网站";
$l['postbit_email'] = "给该用户发送Email";
$l['postbit_find'] = "查找该用户的所有帖子";
$l['postbit_report'] = "向版主举报该帖子";
$l['postbit_quote'] = "在回复中引用该消息";
$l['postbit_qdelete_post'] = "删除帖子";
$l['postbit_qdelete_thread'] = "删除主题";
$l['postbit_qrestore_post'] = "恢复帖子";
$l['postbit_qrestore_thread'] = "恢复主题";
$l['postbit_profile'] = "查看用户个人资料";
$l['postbit_pm'] = "给该用户发送PM";
$l['postbit_edit'] = "编辑帖子";
$l['postbit_multiquote'] = "引用帖子";
$l['postbit_quick_edit'] = "快速编辑";
$l['postbit_full_edit'] = "完整编辑";
$l['postbit_show_ignored_post'] = "显示该帖子";
$l['postbit_currently_ignoring_user'] = "消息内容已隐藏，因为 {1}在您的 <a href=\"usercp.php?action=editlists\">忽略列表上</a>.";
$l['postbit_warning_level'] = "警告等级:";
$l['postbit_warn'] = "警告该帖子的作者";
$l['postbit_purgespammer'] = "垃圾清理";

$l['postbit_button_reputation_add'] = '添加声望';
$l['postbit_button_website'] = '网站';
$l['postbit_button_email'] = 'Email';
$l['postbit_button_find'] = '查找';
$l['postbit_button_report'] = '举报';
$l['postbit_button_quote'] = '回复';
$l['postbit_button_qdelete'] = '删除';
$l['postbit_button_qrestore'] = '恢复';
$l['postbit_button_profile'] = '个人资料';
$l['postbit_button_pm'] = 'PM';
$l['postbit_button_warn'] = '警告';
$l['postbit_button_edit'] = '编辑';
$l['postbit_button_multiquote'] = '引用';
$l['postbit_button_reply_pm'] = '回复';
$l['postbit_button_reply_all'] = '回复全部';
$l['postbit_button_forward'] = '转发';
$l['postbit_button_delete_pm'] = '删除';
$l['postbit_button_purgespammer'] = "垃圾清理";

$l['forumjump'] = "论坛跳转:";
$l['forumjump_select'] = "请选择目的地:";
$l['forumjump_pms'] = "PM";
$l['forumjump_usercp'] = "用户控制面板";
$l['forumjump_wol'] = "查看在线用户";
$l['forumjump_search'] = "搜索";
$l['forumjump_home'] = "论坛首页";

$l['redirect'] = "您将被引导至";
$l['unknown_error'] = "发生了一个未知的错误。";
$l['post_fetch_error'] = '读取帖子发生错误';

$l['smilieinsert'] = "表情";
$l['smilieinsert_getmore'] = "更多";
$l['on'] = "开";
$l['off'] = "关";
$l['unread_report'] = "版主通知: 有 1 条未读的帖子举报。";
$l['unread_reports'] = "版主通知: 有 {1} 条未读的帖子举报。";
$l['pending_joinrequest'] = "组管理员通知：有1个会员加入该组的请求。";
$l['pending_joinrequests'] = "组管理员通知：有 {1} 个会员加入该组的请求。";

$l['year'] = "年";
$l['year_short'] = "y";
$l['years'] = "年";
$l['years_short'] = "y";
$l['month'] = "月";
$l['month_short'] = "m";
$l['months'] = "月";
$l['months_short'] = "m";
$l['week'] = "周";
$l['week_short'] = "w";
$l['weeks'] = "周";
$l['weeks_short'] = "w";
$l['day'] = "天";
$l['day_short'] = "d";
$l['days'] = "天";
$l['days_short'] = "d";
$l['hour'] = "时";
$l['hour_short'] = "h";
$l['hours'] = "时";
$l['hours_short'] ="h";
$l['minute'] = "分";
$l['minute_short'] ="m";
$l['minutes'] = "分";
$l['minutes_short'] = "m";
$l['second'] = "秒";
$l['second_short'] ="s";
$l['seconds'] = "秒";
$l['seconds_short'] = "s";

$l['rel_in'] = "之后 ";
$l['rel_ago'] = "之前";
$l['rel_less_than'] = "少于 ";
$l['rel_time'] = "{1}{2} {3} {4}";
$l['rel_minutes_single'] = "分钟";
$l['rel_minutes_plural'] = "分钟";
$l['rel_hours_single'] = "小时";
$l['rel_hours_plural'] = "小时";

$l['permanent'] = "永久";
$l['save_draft'] = "保存为草稿";
$l['go'] = "继续";
$l['bbclosed_warning'] = "论坛当前为关闭状态.";
$l['banned_warning'] = "您的账户已被封号.";
$l['banned_warning2'] = "封号原因";
$l['banned_warning3'] = "禁令将被解除";
$l['banned_lifted_never'] = "永不";
$l['banned_email_warning'] = "您当前的Email地址不可用，请重设以继续.";
$l['powered_by'] = "Powered By";
$l['copyright'] = "Copyright";
$l['attach_quota'] = "您当前正使用您被分配的附件使用量({2})其中的 <strong>{1}</strong> ";
$l['view_attachments'] = "[查看我的附件]";
$l['unlimited'] = "无限制";

$l['click_hold_edit'] = "(单机并长按以编辑)";

$l['guest_count'] = "1 位游客";
$l['guest_count_multiple'] = "{1} 位游客";

$l['size_yb'] = "YB";
$l['size_zb'] = "ZB";
$l['size_eb'] = "EB";
$l['size_pb'] = "PB";
$l['size_tb'] = "TB";
$l['size_gb'] = "GB";
$l['size_mb'] = "MB";
$l['size_kb'] = "KB";
$l['size_bytes'] = "bytes";

$l['slaps'] = "暴击了";
$l['with_trout'] = "用一个巨型鲑鱼";

$l['mybb_engine'] = "MyBB 引擎";
$l['quickdelete_confirm'] = "确认删除该帖子?";
$l['quickrestore_confirm'] = "确认恢复该帖子?";
$l['newpm_notice_one'] = "<strong>您有1条未读短信</strong> 来自 {1} 标题为 <a href=\"{2}/private.php?action=read&amp;pmid={3}\" style=\"font-weight: bold;\">{4}</a>";
$l['newpm_notice_multiple'] = "<strong>您有{1} 条未读短信.</strong> 最新的一条来自 {2} 标题为 <a href=\"{3}/private.php?action=read&amp;pmid={4}\" style=\"font-weight: bold;\">{5}</a>";
$l['deleteevent_confirm'] = "确认删除该事件?";
$l['removeattach_confirm'] = "确认从当前帖子中移除附件?";

$l['latest_threads'] = "最新主题";

$l['folder_inbox'] = "收件箱";
$l['folder_sent_items'] = "已发送";
$l['folder_drafts'] = "草稿";
$l['folder_trash'] = "垃圾箱";
$l['folder_untitled'] = "未命名文件夹";

$l['standard_mod_tools'] = "标准工具";
$l['custom_mod_tools'] = "自定义工具";

$l['error_loadlimit'] = "服务器超负荷.  请稍后访问.";
$l['error_boardclosed'] = "此公告栏目前已关闭, 原因如下。";
$l['error_banned'] = "抱歉，您已被禁止访问。您无法发帖，阅读或者登录论坛。有问题请联系管理员。";
$l['error_cannot_upload_php_post'] = "无法上传文件 - 超过PHP指令的post_max_size. 请按返回按钮.";
$l['error_database_repair'] = "MyBB 正在自动修复损坏的列表.";

$l['unknown_user_trigger'] = "发生未知错误.";
$l['warnings'] = "发生下列警报:";

$l['ajax_loading'] = "正在加载. <br />请稍候..";
$l['saving_changes'] = "正在保存..";
$l['refresh'] = "刷新";
$l['select_language'] = "快速选择语言";
$l['select_theme'] = "快速选择主题";

$l['invalid_post_code'] = "认证码不匹配. 您是否正确地使用这个功能? 请返回重试一次。";
$l['invalid_captcha'] = "请输入正确的图像验证码以继续. ";
$l['invalid_captcha_verify'] = "图像验证码不正确，请重新输入。";
$l['invalid_captcha_transmit'] = "图像验证失败，请重试。";
$l['captcha_fetch_failure'] = '提取验证码出错。';
$l['question_fetch_failure'] = '提取新的问题出错';
$l['invalid_ayah_result'] = "Are You a Human游戏未完成. 请重试.";

$l['timezone_gmt_minus_1200'] = "(GMT -12:00) 马绍尔群岛";
$l['timezone_gmt_minus_1100'] = "(GMT -11:00) 诺姆，中途岛";
$l['timezone_gmt_minus_1000'] = "(GMT -10:00) 夏威夷，帕皮提";
$l['timezone_gmt_minus_950'] = "(GMT -9:30) 马克萨斯群岛";
$l['timezone_gmt_minus_900'] = "(GMT -9:00) 阿拉斯加";
$l['timezone_gmt_minus_800'] = "(GMT -8:00) 太平洋时间";
$l['timezone_gmt_minus_700'] = "(GMT -7:00) 山地时间";
$l['timezone_gmt_minus_600'] = "(GMT -6:00) 中部时间，墨西哥城";
$l['timezone_gmt_minus_500'] = "(GMT -5:00) 东部时间，波哥大，利马，基多";
$l['timezone_gmt_minus_450'] = "(GMT -4:30) 加拉加斯";
$l['timezone_gmt_minus_400'] = "(GMT -4:00) 大西洋时间，拉巴斯，哈利法克斯";
$l['timezone_gmt_minus_350'] = "(GMT -3:30) 纽芬兰";
$l['timezone_gmt_minus_300'] = "(GMT -3:00) 巴西，布宜诺斯艾利斯，乔治敦，福克兰群岛";
$l['timezone_gmt_minus_200'] = "(GMT -2:00) 中大西洋南乔治亚岛和南桑威奇群岛";
$l['timezone_gmt_minus_100'] = "(GMT -1:00) 亚速尔群岛，佛得角群岛";
$l['timezone_gmt'] = "(GMT) 卡萨布兰卡，都柏林，爱丁堡，伦敦，里斯本，蒙罗维亚";
$l['timezone_gmt_100'] = "(GMT +1:00) 柏林，布鲁塞尔，哥本哈根，马德里，巴黎，罗马，华沙w";
$l['timezone_gmt_200'] = "(GMT +2:00) 雅典，伊斯坦布尔，开罗，耶路撒冷，南非";
$l['timezone_gmt_300'] = "(GMT +3:00) 加里宁格勒，明斯克，巴格达，利雅得，内罗毕";
$l['timezone_gmt_350'] = "(GMT +3:30) 德黑兰";
$l['timezone_gmt_400'] = "(GMT +4:00) 莫斯科，阿布扎比，巴库，马斯喀特，第比利斯";
$l['timezone_gmt_450'] = "(GMT +4:30) 喀布尔";
$l['timezone_gmt_500'] = "(GMT +5:00) 伊斯兰堡，卡拉奇，塔什干";
$l['timezone_gmt_550'] = "(GMT +5:30) 孟买，加尔各答，马德拉斯，新德里";
$l['timezone_gmt_575'] = "(GMT +5:45) 加德满都";
$l['timezone_gmt_600'] = "(GMT +6:00) 阿拉木图，Dhakra，叶卡捷琳堡";
$l['timezone_gmt_650'] = "(GMT +6:30) 仰光";
$l['timezone_gmt_700'] = "(GMT +7:00) 曼谷，河内，雅加达";
$l['timezone_gmt_800'] = "(GMT +8:00) 北京，香港，帕斯，新加坡，台北，马尼拉";
$l['timezone_gmt_900'] = "(GMT +9:00) 大阪，札幌，首尔，东京，伊尔库次克";
$l['timezone_gmt_950'] = "(GMT +9:30) 阿德莱德，达尔文";
$l['timezone_gmt_1000'] = "(GMT +10:00) 墨尔本，巴布亚新几内亚，悉尼，雅库茨克";
$l['timezone_gmt_1050'] = "(GMT +10:30) 豪勋爵岛";
$l['timezone_gmt_1100'] = "(GMT +11:00) 马加丹，新喀里多尼亚，所罗门群岛，符拉迪沃斯托克";
$l['timezone_gmt_1150'] = "(GMT +11:30) 诺福克岛";
$l['timezone_gmt_1200'] = "(GMT +12:00) 奥克兰，惠灵顿，斐济，马绍尔群岛";
$l['timezone_gmt_1275'] = "(GMT +12:45) 查塔姆群岛";
$l['timezone_gmt_1300'] = "(GMT +13:00) 萨摩亚，汤加，托克劳";
$l['timezone_gmt_1400'] = "(GMT +14:00) 莱恩群岛";
$l['timezone_gmt_short'] = "GMT {1}({2})";

$l['missing_task'] = "错误: 任务文件不存在";
$l['task_backup_cannot_write_backup'] = "错误: 数据库备份任务无法写入备份目录";
$l['task_backup_ran'] = "数据库备份任务成功运行";
$l['task_checktables_ran'] = "数据表检查任务成功运行, 未发现坏掉的表。";
$l['task_checktables_ran_found'] = "注意: 数据表检查任务成功运行了, 修复了 {1} 个表。";
$l['task_dailycleanup_ran'] = "每日清理任务成功运行。";
$l['task_hourlycleanup_ran'] = "每小时清理任务成功运行。";
$l['task_logcleanup_ran'] = "日志清理任务成功运行, 且剪除了所有旧日志。";
$l['task_promotions_ran'] = "晋级任务成功运行。";
$l['task_threadviews_ran'] = "主题人气成功更新。";
$l['task_usercleanup_ran'] = "会员清理任务成功运行。";
$l['task_massmail_ran'] = "群发邮件任务成功运行。";
$l['task_userpruning_ran'] = "用户修剪任务成功运行。";
$l['task_delayedmoderation_ran'] = "延时的处理任务成功运行。";
$l['task_massmail_ran_errors'] = "一个或多个问题发生，在发送给 \"{1}\":
{2}";
$l['task_versioncheck_ran'] = "版本验证任务成功运行.";
$l['task_versioncheck_ran_errors'] = "无法连接到MyBB以验证版本.";
$l['task_recachestylesheets_ran'] = '重新缓存{1}样式表.';

$l['dismiss_notice'] = "解除这条注意事项";

$l['next'] = "下一页";
$l['previous'] = "上一页";
$l['delete'] = "删除";

$l['massmail_username'] = "用户名";
$l['email_addr'] = "Email 地址";
$l['board_name'] = "论坛名称";
$l['board_url'] = "论坛网址";

$l['comma'] = ", ";

$l['debug_generated_in'] = "生成时间 {1}";
$l['debug_weight'] = "({1}% PHP / {2}% {3})";
$l['debug_sql_queries'] = "SQL查询: {1}";
$l['debug_server_load'] = "服务器负载: {1}";
$l['debug_memory_usage'] = "内存使用: {1}";
$l['debug_advanced_details'] = "高级详情";

$l['error_emailflooding_1_second'] = "抱歉， 每 {1} 分钟只能发送一封email。请等待1秒后重试";
$l['error_emailflooding_seconds'] = "抱歉， 每 {1} 分钟只能发送一封email。请等待 {2} 秒后重试";
$l['error_emailflooding_1_minute'] = "抱歉， 每 {1} 分钟只能发送一封email。请等待1分钟后重试";
$l['error_emailflooding_minutes'] = "抱歉， 每 {1} 分钟只能发送一封email。请等待{2}分钟后重试";
$l['error_invalidfromemail'] = "您没有输入一个有效的email地址";
$l['error_noname'] = "名字无效.";
$l['your_email'] = "您的Email:";
$l['email_note'] = "在此输入您的Email.";
$l['your_name'] = "您的名字:";
$l['name_note'] = "在此输入您的名字.";

$l['january'] = "一月";
$l['february'] = "二月";
$l['march'] = "三月";
$l['april'] = "四月";
$l['may'] = "五月";
$l['june'] = "六月";
$l['july'] = "七月";
$l['august'] = "八月";
$l['september'] = "九月";
$l['october'] = "十月";
$l['november'] = "十一月";
$l['december'] = "十二月";

$l['moderation_forum_attachments'] = "请注意本论坛新增的附件需要版主审核才能访问";
$l['moderation_forum_posts'] = "请注意本论坛新增的帖子需要版主审核才能访问";
$l['moderation_user_posts'] = "请注意你的新帖子需要版主审核才能访问";
$l['moderation_forum_thread'] = "请注意本论坛新增的主题需要版主审核才能访问";
$l['moderation_forum_edits'] = "请注意编辑过的帖子需要版主审核才能访问";
$l['moderation_forum_edits_quick'] = "请注意编辑过的帖子需要版主审核才能访问";
$l['awaiting_message_single'] = "有1个账户尚未激活，请前往ACP面板激活用户";
$l['awaiting_message_plural'] = "有 {1} 个账户尚未激活，请前往ACP面板激活用户";

$l['select2_match'] = "找到1个结果, 请输入并选择.";
$l['select2_matches'] = "找到{1}个结果 , 使用上下键来导航.";
$l['select2_nomatches'] = "未找到匹配";
$l['select2_inputtooshort_single'] = "请输入1个或更多字符";
$l['select2_inputtooshort_plural'] = "请输入{1}个或更多字符";
$l['select2_inputtoolong_single'] = "请删除1个字符";
$l['select2_inputtoolong_plural'] = "请删除{1}个字符";
$l['select2_selectiontoobig_single'] = "您只能选择1个项目";
$l['select2_selectiontoobig_plural'] = "您只能选择{1}个项目";
$l['select2_loadmore'] = "加载更多结果…";
$l['select2_searching'] = "正在查找…";

