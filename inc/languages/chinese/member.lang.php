<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['nav_register'] = "注册";
$l['nav_activate'] = "激活";
$l['nav_resendactivation'] = "重发激活说明邮件";
$l['nav_lostpw'] = "恢复忘记的密码";
$l['nav_resetpassword'] = "重置密码";
$l['nav_login'] = "登录";
$l['nav_emailuser'] = "Email 用户";
$l['nav_profile'] = "{1} 的个人资料";

$l['tpp_option'] = "每页显示 {1} 个主题";
$l['ppp_option'] = "每页显示 {1} 个帖子";
$l['account_activation'] = "帐号激活";
$l['activate_account'] = "激活帐号";
$l['activation_code'] = "激活认证码";

$l['email_user'] = "给 {1} 寄送 Email";
$l['email_subject'] = "Email 标题";
$l['email_message'] = "Email 内容";
$l['send_email'] = "发送 Email";
$l['error_hideemail'] = "收件人选择隐藏他/她的 Email 地址, 因此您无法寄信给他/她。";
$l['error_no_email_subject'] = "您需要为您的 Email 输入标题";
$l['error_no_email_message'] = "您需要为您的 Email 输入内容";

$l['login'] = "登录";
$l['pw_note'] = "请注意密码是区分大小写的。";
$l['lostpw_note'] = "忘了您的密码?";
$l['lost_pw'] = "忘记帐号密码";
$l['lost_pw_form'] = "恢复忘记密码的表单";
$l['email_address'] = "Email 地址:";
$l['request_user_pass'] = "发回用户名/密码";
$l['profile'] = "{1} 的个人资料";
$l['registration_date'] = "注册日期:";
$l['date_of_birth'] = "出生日期:";
$l['birthdayhidden'] = "隐藏";
$l['birthday'] = "生日:";
$l['local_time'] = "当地时间:";
$l['local_time_format'] = "{1} {2}";
$l['users_forum_info'] = "{1} 的论坛信息";
$l['joined'] = "注册加入：";
$l['lastvisit'] = "最近访问：";
$l['total_posts'] = "帖子总数:";
$l['ppd_percent_total'] = "每天 {1} 条 | 约占总帖子的 {2}%";
$l['total_threads'] = "主题总数:";
$l['tpd_percent_total'] = "每天 {1} 篇 | 约占总主题的 {2}%";
$l['find_posts'] = "查看他的全部帖子";
$l['find_threads'] = "查看他的全部主题";
$l['members_referred'] = "推荐会员：";
$l['rating'] = "评分:";
$l['users_contact_details'] = "{1} 的联系方式";
$l['homepage'] = "主页:";
$l['pm'] = "PM:";
$l['send_pm'] = "给 {1} 发送PM。";
$l['icq_number'] = "ICQ 号码:";
$l['aim_screenname'] = "AIM 用户名称:";
$l['yahoo_id'] = "Yahoo ID:";
$l['skype_id'] = "Skype ID:";
$l['google_id'] = "Google Talk ID:";
$l['avatar']  = "头像:";
$l['warning_level'] = "警告等级:";
$l['warn'] = "警告";
$l['away_note'] = "{1} 现在处于离开状态。";
$l['away_reason'] = "理由:";
$l['away_since'] = "离开自从:";
$l['away_returns'] = "回来时间:";
$l['away_no_reason'] = "没指定。";
$l['users_additional_info'] = "关于 {1} 的其它信息";
$l['email'] = "Email:";
$l['send_user_email'] = "给 {1} 发送 Email。";
$l['users_signature'] = "{1} 的签名档";
$l['agreement'] = "用户协议";

$l['agreement_1'] = "
第一章  总则 
   第1条   作为新生你是否曾在百度、facebook等处搜寻租房、接机等信息而不得；作为在校生你是否疲于翻阅各种微信群，以及各种求拉微信群而苦恼；本社区由计算机系博士生、研究生为解决微信等及时通讯的信息孤岛问题而研发的信息发布、分享的公益平台。为规范本社区的信息服务，保障社区信息服务的正常运行和健康发展，维护有关各方的正当权益，更好地为同学们服务，特制定本办法。
   第2条   本社区名为“嗨~哟~喂！特拉华大学社区”。以下简称本社区。本社区以提供有价值的信息交流服务为基本导向，信息交流的范围涵盖生活休闲、人文、文体娱乐、学术、技术、等各领域。";

$l['agreement_2'] = "
第二章  社区用户
  
   第1条  于本社区注册并通过本社区身份认证者为本社区合格使用者。 
   第2条  本社区使用者在注册时应填写UD邮箱或其他证明自己身份的信息。使用不雅或不恰当的 帐号名称者，本社区将拒绝通过认证或删除已注册帐号。本社区使用者的注册资料经认定为虚假资料者立即删除其帐号。 
   第3条  本社区合格使用者享受其应有的一切权利及使用本社区资源，同时须遵 守本社区的管理规则。本社区合格使用者在个人资料上受到本社区保护。本社区不接受任何个人或单位查询他人基本资料的要求，政府部门也不例外。 
   第4条  本社区使用者言行不得违反中美两国的相关法律、法规及本社区 的各项规章制度。对违反者，本社区保留停止其帐号部分权限、直至删除帐号的权力。本社区将采取一切可能手段制止使用者危及社区生存的行为。";

$l['agreement_3'] = "
第三章  版权声明 
  
   第1条  本章所声明版权之作品包含其所承载的全部信息，其信息载体包括但不限于文章或图片。本社区使用者在本社区发表的所有作品仅代表作者本人观点与本社区立场无关。作者文责自负。 
   第2条  对于本社区的合格使用者在本社区公开发表的任何作品，本社区有权用于本社区内其他用途。原作品有附带版权声明者除外。本社区在使用时对作品的修改应保持作者原意并征求原作者同意。 
   第3条  在本社区发表的所有作品版权仅归原作者所有，若作者有版权声明或原作从其它网站转载而附带有原版权声明者，其版权归属以附带声明为准。 
   第4条  任何转载或引用原作发表于本社区的版权作品须符合以下规范： 
             一、用于非商业、非盈利、非广告性目的时需注明作者及作品出处为嗨哟喂社区。 
             二、用于商业、盈利、广告性目的时需征得作品原作者的同意，并注明作者姓名、授权范围及原作出处嗨哟喂社区。 
             三、任何文章或图片的修改或删除均应保持作者原意并征求原作者同意，并注明授权范围。";

$l['agreement_4'] = "
第四章  责任声明 
  
   第1条  本社区使用者之间通过社区相识、交往中所发生或可能发生的任何心理、生理上的伤害和经济上的损失，本社区不承担任何责任。 
   第2条  本社区使用者在使用本社区资源时因违反本社区管理规则而触犯中美两国法律、法规的，一切后果自行负责，本社区不承担任何责任。
   第3条  由于非故意及不可预期与抗拒的事件，包括但不限于硬件故障、系统维护或升级，导致的一切不便与损失，包括但不限于用户数据丢失、服务暂停或停止，社区不承担赔偿及其他连带的法律责任。 
   第4条  本社区保护合格使用者私人信件的隐私权。但本社区合格使用者的私人信件请自行保存，本社区不负保管责任。本社区保留因需要而清理本社区使用者私人信件的权利。系统若遇不可抗拒外力影响而导致资料遗失，本社区亦不负责。";

$l['agreement_5'] = "
第五章  附则 
  
   第1条  本管理规则未涉及的问题参见中美两国有关法律、法规，当本管理规则与法律、法规冲突时，以法律、法规为准。
   第2条  本规则的修改及解释权由嗨哟喂特拉华大学社区负责。 
   第3条  本规则自公布之日施行。
												2018.8.1修订";

$l['registration'] = "注册";
$l['required_fields'] = "必填";
$l['complex_password'] = "<acronym title=\"密码至少 {1} 个字符并且要包含大写字母、小写字母和数字。\">复杂</acronym> 密码：";
$l['confirm_email'] = "确认 Email:";
$l['optional_fields'] = "选填的项目";
$l['website_url'] = "您的网站网址:";
$l['birthdate'] = "出生日期:";
$l['additional_info'] = "其它信息";
$l['required_info'] = "必要信息";
$l['i_agree'] = "我同意";
$l['account_details'] = "帐号详细资料";
$l['account_prefs'] = "帐号偏好设定(选项):";
$l['invisible_mode'] = "在 \"谁在线上\" 名单列表中隐藏我的名字。";
$l['allow_notices'] = "接受来自管理员的 Email。";
$l['hide_email'] = "对其它会员隐藏 Email 地址。";
$l['email_notify'] = "自动订阅您发布的主题。";
$l['receive_pms'] = "接收来自其它用户的PM。";
$l['pm_notice'] = "当我收到PM时提醒我。";
$l['email_notify_newpm'] = "当我收到PM时发送 Email 通知我。";
$l['time_offset'] = "时区 (<acronym title=\"Daylight Saving Time\">DST(夏令时)</acronym> 的修正除外):";
$l['time_offset_desc'] = "如果您的居住地和本论坛设定的时区不同, 您可以从下面列表中选择自己合适的时区。";
$l['dst_correction'] = "日光节约时制(DST, 夏令时)修正:";
$l['dst_correction_auto'] = "自动检测 DST 设定";
$l['dst_correction_enabled'] = "总是使用 DST 修正";
$l['dst_correction_disabled'] = "从不使用 DST 修正";
$l['redirect_registered_coppa_activate'] = "感谢您在 {1} 注册, {2}. 您的帐号已经成功创建, 可是这个帐号的所有者在 13 岁以下, 在使用这个帐号之前必须先寻求父母(或监护人)的许可.<br /><br />家长或法定监护人需要下载并填写 <a href=\"member.php?action=coppa_form\">COPPA 承诺&amp;许可表</a> 并提交给我们.<br /><br />一旦我们收到这个许可表的完整复件, 这个帐号就会被激活。";
$l['coppa_compliance'] = "COPPA 承诺";
$l['coppa_desc'] = "在这个论坛注册, 我们必须检验您的年龄以遵守 <a href=\"http://coppa.org/\" title=\"Children's Online Privacy Protection Act\" target=\"_blank\">COPPA</a> 约定. 请在下面输入您的出生日期.<br /><br />如果您未满 13 岁, 在完成注册之前必须先征得家长的许可. 家长或法定监护人需要下载填写 <a href=\"member.php?action=coppa_form\" target=\"_blank\">COPPA 承诺&amp;许可表</a> 并提交给我们。";
$l['hide_dob'] = "您可以在注册之后通过修改您的个人资料选择隐藏您的出生日期和年龄。";
$l['signature'] = "签名档:";
$l['continue_registration'] = "继续注册";
$l['birthdayprivacy'] = "出生日期隐私:";
$l['birthdayprivacyall'] = "显示年龄和出生日期";
$l['birthdayprivacynone'] = "隐藏年龄和出生日期";
$l['birthdayprivacyage'] = "只显示年龄";
$l['leave_this_field_empty'] = "留空:";
$l['error_need_to_be_thirteen'] = "在这个论坛注册要求您必须满 13 岁。";
$l['coppa_registration'] = "COPPA 注册表";
$l['coppa_form_instructions'] = "请打印这个表单并填写好, 然后传真到下面的号码或邮寄到所提供的地址。";
$l['fax_number'] = "传真号码:";
$l['mailing_address'] = "邮寄地址:";
$l['account_information'] = "帐号信息";
$l['parent_details'] = "家长/监护人资料";
$l['full_name'] = "全名:";
$l['relation'] = "关系:";
$l['phone_no'] = "电话 #:";
$l['coppa_parent_agreement'] = "我保证我提供的信息是真实有效的, 这些信息将来可能发生改变. 可以通过输入您在下面提供的密码请求删除这个帐号。";

$l['coppa_agreement_1'] = "13 岁以下的用户若要在 {1} 注册, 必须收到他们的家长或法定监护人的许可。";
$l['coppa_agreement_2'] = "在会员资格获得批准之前, 家长或法定监护人需要下载填写 <a href=\"member.php?action=coppa_form\" target=\"_blank\">COPPA 承诺&amp;许可表</a> 并提交给我们.。";
$l['coppa_agreement_3'] = "如果您愿意您可以现在就开始注册过程, 不过在上述的承诺表收到之前您的帐号是不可用的。";

$l['error_invalid_birthday'] = '生日无效. 请输入有效的生日.';
$l['error_awaitingcoppa'] = "您无法以这个帐号身份登录, 因为我们还在等待来自家长/监护人 COPPA 确认.<br /><br />家长或法定监护人需要下载并填写 <a href=\"member.php?action=coppa_form\">COPPA 承诺&amp;许可表</a> 并提交给我们.<br /><br />一旦我们收到这个许可表的完整复印, 这个帐号就会被激活。";

$l['lang_select'] = "语言设定:";
$l['lang_select_desc'] = "本社区目前支持简体中文和英文，请选择您习惯的语种";
$l['lang_select_default'] = "使用默认";

$l['submit_registration'] = "提交注册!";
$l['confirm_password'] = "确认密码:";
$l['referrer'] = "推荐人:";
$l['referrer_desc'] = "如果您是另一个论坛会员介绍您来这个论坛的, 您可以在下面填入他的用户名. 如果不是, 请直接留空。";
$l['search_user'] = "查找用户";
$l['resend_activation'] = "重发帐号激活";
$l['request_activation'] = "申请激活码";
$l['ppp'] = "每页显示的帖子数:";
$l['ppp_desc'] = "允许您选择在阅读主题时每页显示的帖子数量。";
$l['tpp'] = "每页显示的主题数:";
$l['tpp_desc'] = "允许您选择在主题列表时每页显示的主题数量。";
$l['reset_password'] = "重置密码";
$l['send_password'] = "寄送新密码!";
$l['image_verification'] = "图片验证码";
$l['verification_note'] = "请在下面文本框内输入右边图片中包含的文字. 这个步骤用于防止自动注册。";
$l['verification_subnote'] = "(不区分大小写)";
$l['registration_errors'] = "您的注册过程中发生了以下错误:";
$l['timeonline'] = "在线时间:";
$l['timeonline_hidden'] = "(隐藏)";
$l['registrations_disabled'] = "抱歉, 由于管理员禁用新帐号注册, 您现在不能注册。";
$l['error_username_length'] = "您的用户名称是无效的. 用户名必须在 {1} 到 {2} 个字符之间。";
$l['error_stop_forum_spam_spammer'] = '抱歉, 您的Email或者IP已被标记为垃圾并已被阻止， 如有问题请联系管理员。';
$l['error_stop_forum_spam_fetching'] = '抱歉，消息验证出错，无法连接垃圾消息数据库，请稍后再试。';

$l['none_registered'] = "未注册";
$l['not_specified'] = "未指定";
$l['membdayage'] = "({1} 岁)";
$l['mod_options'] = "管理选项";
$l['edit_in_mcp'] = "在版主控制面板中编辑这个用户";
$l['ban_in_mcp'] = "在版主控制面板中禁止这个用户";
$l['purgespammer'] = "列为垃圾用户";
$l['edit_usernotes'] = "在版主控制面板中编辑用户备注";
$l['no_usernotes'] = "该用户当前没有备注";
$l['view_all_notes'] = "查看所有备注";
$l['view_notes_for'] = "查看 {1} 的备注";
$l['reputation'] = "声望:";
$l['reputation_vote'] = "评分";
$l['reputation_details'] = "详情";
$l['already_logged_in'] = "注意: 您当前已经是登录状态了, 帐号为: {1}。";
$l['admin_edit_in_acp'] = "在管理员控制面板编辑这个用户";
$l['admin_ban_in_acp'] = "在管理员控制面板封禁这个用户";
$l['admin_options'] = "管理员选项";

$l['redirect_registered_activation'] = "感谢您在 {1} 注册, {2}.<p>为了完成您的注册, 请检查您的 Email 查看帐号激活说明. 在您激活帐号之前, 您可能无法在这里的论坛发帖";
$l['redirect_emailupdated'] = "您的 Email 地址已经成功修改了.<br />现将转入论坛主页。";
$l['redirect_accountactivated'] = "您的帐号已经成功激活了.<br />现将转入论坛主页。";
$l['redirect_accountactivated_admin'] = "您的Email已验证成功.<br />您的注册身份已激活为管理员. 到那时，您可能无法在某些论坛发帖.<br />现将转入论坛首页.";
$l['redirect_registered'] = "谢谢您在 {1}, {2} 的注册.<br />现将转入论坛首页.";
$l['redirect_registered_admin_activate'] = "谢谢您在 {1}, {2} 的注册.<br />您的注册身份已激活为管理员. 到那时，您可能无法在某些论坛发帖.";
$l['redirect_loggedout'] = "您已成功注销.<br />现将返回到论坛首页.";
$l['redirect_alreadyloggedout'] = "您已经退出登录或还没有登录.<br />现将返回到论坛首页。";
$l['redirect_lostpwsent'] = "非常感谢, 所有和那个 Email 有关的帐号都将收到一封详细介绍如何重置密码的电子邮件.<br /><br />现将转入论坛首页。";
$l['redirect_activationresent'] = "您的激活说明 Email 已经重新寄出。";
$l['redirect_passwordreset'] = "非常感谢, 您的帐号的密码已经重置. 随机生成的新密码已经寄送到您帐号里设定的 Email 里。";
$l['redirect_memberrated'] = "已经成功评价这个用户(会员)。";
$l['redirect_registered_passwordsent'] = "一个随机生成的密码已经发送到您的 Email 地址里. 在您登录论坛之前您必须先去您的收取 Email 并查看这个密码。";
$l['redirect_validated'] = "非常感谢, 您的帐号已经有效.<br />您将转到论坛中。";

$l['error_activated_by_admin'] = "所有的注册均必须等待管理员的审核批准, 因此您不能重新寄出帐号激活邮件。";
$l['error_alreadyregistered'] = "抱歉, 我们的系统显示您已经在这个论坛注册过了, 然而重复注册多个帐号是不被允许的。";
$l['error_alreadyregisteredtime'] = "我们无法处理您的注册, 因为在刚过去的 {2} 小时里, 已经有 {1} 个新帐号使用与您相同的 IP 地址注册. 请稍后再试。";
$l['error_badlostpwcode'] = "似乎您输入了一个无效的密码重设验证码. 请重新阅读您收到的 Email 或联系论坛管理员获得更多帮助。";
$l['error_badactivationcode'] = "您输入了一个无效的帐号激活验证码. 若要重新寄送激活说明 Email, 请点击<a href=\"member.php?action=resendactivation\">这儿</a>。";
$l['error_alreadyactivated'] = "您的帐号似乎已经激活了或者不需要 Email 验证。";
$l['error_alreadyvalidated'] = "您的Email已通过验证.";
$l['error_nothreadurl'] = "您的内容没有包含主题的网址网址. 请使用 \"发送给朋友\" 功能来达到目的。";
$l['error_bannedusername'] = "您输入了一个被禁止注册的用户名. 请另选一个用户名。";
$l['error_notloggedout'] = "退出登录时, 您的用户 ID 无法完成校验. 这可能是由于一个恶意的 Javascript 在尝试自动迫使您退出登录. 如果您有意退出登录, 请点击顶部菜单里的 \"退出\" 按钮。";
$l['error_regimageinvalid'] = "您输入的图片验证码不正确. 请准确地输入图片中显示的验证码。";
$l['error_regimagerequired'] = "请输入验证码以继续登陆。";
$l['error_spam_deny'] = "系统检测出您可能是垃圾发送者所以禁止您注册，如有疑问请联系管理员。";
$l['error_spam_deny_time'] = "系统检测出您可能是垃圾发送者所以禁止您注册.  注册间隔最短时间为{1}秒，  您的注册间隔时间为{2}秒. 如有疑问请联系管理员。";

$l['js_validator_no_username'] = "您必须输入用户名称";
$l['js_validator_invalid_email'] = "您必须输入一个有效的 Email 地址";
$l['js_validator_email_match'] = "您需要再次输入相同的 Email 地址";
$l['js_validator_no_image_text'] = "您需要输入上图中的文字";
$l['js_validator_no_security_question'] = "您需要输入上面问题的答案";
$l['js_validator_password_matches'] = "您输入的密码必须相匹配";
$l['js_validator_password_complexity'] = "检查密码复杂度";
$l['js_validator_password_length'] = "您的密码至少需要 {1} 个字符";
$l['js_validator_not_empty'] = "您必须为该项选取或输入一个值";
$l['js_validator_checking_username'] = "正在检测用户名是否可用";
$l['js_validator_username_length'] = "用户名必须在 {1} 到 {2} 个字符之间";
$l['js_validator_checking_referrer'] = "正在检测推荐人的用户名是否存在。";
$l['js_validator_captcha_valid'] = "正在检测您输入的图片验证码是否正确。";

$l['security_question'] = "安全问题";
$l['question_note'] = "请回答问题，此举是为了防止自动注册.";
$l['error_question_wrong'] = "答案不正确. 请重新输入.";

$l['subscription_method'] = "默认的主题订阅模式:";
$l['no_auto_subscribe'] = "不订阅";
$l['no_subscribe'] = "不需要通知";
$l['instant_email_subscribe'] = "即时Email通知";
$l['instant_pm_subscribe'] = "即时PM通知";

$l['remove_from_buddy_list'] = "从好友名单中移除";
$l['add_to_buddy_list'] = "添加到好友列表";
$l['remove_from_ignore_list'] = "从忽略名单中移除";
$l['add_to_ignore_list'] = "添加到忽略名单";
$l['report_user'] = "举报用户";

$l['newregistration_subject'] = "在{1}时有新的注册";
$l['newregistration_message'] = "{1},

在 {2} 时有新的注册申请，需要等待管理员激活.

用户名: {3}

谢谢,
{2} 成员";
