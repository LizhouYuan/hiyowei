<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

/*
 * Custom Help Document Translation Format
 *
 * // Help Document {hid}
 * $l['d{hid}_name'] = "文档名";
 * $l['d{hid}_desc'] = "文档描述";
 * $l['d{hid}_document'] = "文档内容";
 */
