<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

/*
 * Custom Help Section Translation Format
 *
 * // Help Section {sid}
 * $l['s{sid}_name'] = "章节名称";
 * $l['s{sid}_desc'] = "章节描述";
 */
