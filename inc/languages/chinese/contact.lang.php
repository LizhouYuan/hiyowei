<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['contact'] = '联系我们';
$l['contact_no_message'] = '发送的讯息为空';
$l['contact_no_subject'] = '需要输入一个标题';
$l['contact_no_email'] = '必须输入一个有效的email地址';
$l['contact_success_message'] = '消息已经成功发送到管理员';
$l['contact_subject'] = '标题';
$l['contact_subject_desc'] = '简要描述你的讯息';
$l['contact_message'] = '正文';
$l['contact_message_desc'] = '在此区域详细描述相关内容';
$l['contact_email'] = 'Email';
$l['contact_email_desc'] = '输入你的email以便我们回访';
$l['contact_send'] = '发送';
$l['image_verification'] = "图片验证码";
$l['verification_note'] = "请在下面文本框内输入右边图片中包含的文字. 这个步骤用于防止自动注册。";
$l['verification_subnote'] = "(不区分大小写)";
$l['invalid_captcha'] = "您输入的图片验证码不正确, 请准确地输入图片中显示的验证码。";
$l['subject_too_long'] = '标题太长了， 请输入少于 {1}个字符的标题 (当前 {2})';
$l['message_too_short'] = '帖子内容太短了, 请输入至少 {1} 个字符(当前 {2})';
$l['message_too_long'] = '帖子内容太长了, 请输入少于 {1} 个字符的内容(当前 {2})';

$l['error_stop_forum_spam_spammer'] = '对不起，您的email或IP地址因滥用服务已被列入黑名单，因此您的请求被阻止了';
$l['error_stop_forum_spam_fetching'] = '对不起，在Spammer数据库中审核您的信息失败，常见为数据库无法访问，请稍候再试。';

