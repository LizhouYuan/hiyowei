<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['nav_postpoll'] = "发起投票";
$l['nav_editpoll'] = "编辑投票";
$l['nav_pollresults'] = "投票结果";

$l['edit_poll'] = "编辑投票";
$l['delete_poll'] = "删除投票";
$l['delete_q'] = "删除？";
$l['delete_note'] = "若要删除这个投票, 请选中左边的复选框然后点击右边的按钮。";
$l['delete_note2'] = "<b>注意:</b> 投票一旦被删除将无法恢复。";
$l['question'] = "问题:";
$l['num_options'] = "选项个数:";
$l['max_options'] = "最多:";
$l['poll_options'] = "投票选项:";
$l['update_options'] = "更新选项";
$l['poll_options_note'] = "投票选项应该简短扼要。";
$l['options'] = "短信选项:";
$l['option_multiple'] = "<b>允许多选:</b> 用户可以选择多个选项。";
$l['option_multiple_maxoptions'] = "单用户最大可选数(0为无限制)";
$l['option_public'] = "<b>公开投票:</b> 可以让用户看到谁投了哪个选项。";
$l['option_closed'] = "<b>关闭投票:</b> 如果选中将不允许再投票。";
$l['poll_timeout'] = "投票截止时间:";
$l['timeout_note'] = "允许用户对此投票的天数.<br />(设为 0 将永久能投)";
$l['days_after'] = "几天以后:";
$l['update_poll'] = "更新投票";
$l['option'] = "选项";
$l['votes'] = "票数:";
$l['post_new_poll'] = "发布投票";
$l['days'] = "天";
$l['poll_results'] = "投票结果";
$l['poll_total'] = "总计:";
$l['poll_votes'] = "票(s)";

$l['redirect_pollposted'] = "您的投票已发布.<br />现在将返回主题帖。";
$l['redirect_pollpostedmoderated'] = "您的投票已发布,  但是你的主题正在等候审核.<br />现在将返回论坛版面。";
$l['redirect_pollupdated'] = "投票已更新.<br />现在将返回主题帖。";
$l['redirect_votethanks'] = "感谢您的投票.<br />现在将返回主题帖。";
$l['redirect_unvoted'] = "你在该主题的投票已经移除。<br />现将返回到该主题。";
$l['redirect_polldeleted'] = "非常感谢, 投票已经成功地从主题中移除.<br />现在将返回主题帖。";

$l['error_polloptiontoolong'] = "你设置的一个或多个投票选项超过允许的最大长度, 请返回并缩短它们。";
$l['error_noquestionoptions'] = "你没有设置投票的问题或者选项个数不够, 一个投票最少需要设置2个选项.<br />请返回并修正这个错误。";
$l['error_pollalready'] = "该主题已经包含有投票!";
$l['error_nopolloptions'] = "您指定的投票选项无效或不存在。";
$l['error_maxpolloptions'] = "你选择了太多投票项，只允许选择 {1} 项。<br />请返回并重试";
$l['error_alreadyvoted'] = "这项投票您已经投过。";
$l['error_notvoted'] = "在此次调查中您还没有投票。";
$l['error_invalidpoll'] = "指定的投票无效或不存在。";
$l['error_pollclosed'] = "由于投票已关闭, 您不能投票。";
$l['poll_time_limit'] = "很抱歉，您不能添加一个投票选项到主题。管理员设置了投票只能在{1}小时内增加选项";

$l['poll_deleted'] = "删除投票";
$l['poll_edited'] = "编辑投票";
