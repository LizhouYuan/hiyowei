<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['forum'] = "版块：";
$l['printable_version'] = "可打印的版本";
$l['pages'] = "页:";
$l['thread'] = "主题：";
