<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['nav_reputation'] = "声望报告";
$l['nav_profile'] = "{1} 的个人资料";
$l['reputation_disabled'] = "您不能使用声望系统, 因为管理员已经禁用了这个功能。";

$l['reputation'] = "声望评价";
$l['error'] = "错误";
$l['add_no_uid'] = "您没有指定要评价声望的用户。";
$l['add_no_permission'] = "您没有权限给用户做出声望评价。";
$l['add_disabled'] = "您无法给这个组的用户添加声望评价。";
$l['add_yours'] = "您无法给自己添加声望评价。";
$l['add_invalidpower'] = "您选择的评价选项是无效的, 请从列表中选择。";
$l['add_maxperday'] = "您给出的声望评价次数已经达到今日上限。";
$l['add_maxperuser'] = "今天，您已经给该用户足够多的声誉评级。";
$l['add_maxperthread'] = "今天，您已经给该用户足够多的声誉评级（同一个主题中）。";
$l['add_no_comment'] = "您必须输入一个合适的理由(评论，至少10个字符), 才能做出评价。";
$l['add_toolong'] = "您必须输入一个不超过 {1} 个字符的理由。";
$l['add_negative_disabled'] = "管理员已经禁用负面声望.";
$l['add_neutral_disabled'] = "管理员已经禁用中性声望.";
$l['add_positive_disabled'] = "管理员已经禁用正面声望.";
$l['add_all_rep_disabled'] = "管理员已经禁用所有声望类型. 您无法评价该成员.";
$l['reputation_report'] = "{1} 的声望报告";
$l['reputation_members'] = "来自会员的声望:";
$l['reputation_posts'] = "来自帖子的声望:";
$l['summary'] = "概要";
$l['total_reputation'] = "总声望";
$l['post_reputation'] = "帖子声望";
$l['positive_count'] = "正面";
$l['neutral_count'] = "中性";
$l['negative_count'] = "负面";
$l['last_week'] = "上个星期";
$l['last_month'] = "上一个月";
$l['last_6months'] = "最近 6 个月";
$l['all_time'] = "所有时段";
$l['comments'] = "评论";
$l['close_window'] = "关闭窗口";
$l['add_reputation_vote'] = "给 {1} 添加声望评价";
$l['add_reputation_to_post'] = "这个声望是为 {1} 的帖子。<br />";
$l['neg_rep_disabled'] = "<span class=\"smalltext\">* - <em>负面声望当前已被禁用</em></span>";
$l['pos_rep_disabled'] = "<span class=\"smalltext\">* - <em>正面声望当前已被禁用</em></span>";
$l['neu_rep_disabled'] = "<span class=\"smalltext\">* - <em>中性声望当前已被禁用</em></span>";
$l['no_comment_needed'] = "您正在添加声望，由于这个用户的帖子链接到他的个人资料。评论不是必需的，但如果您要离开的，请在下面输入。<br />";
$l['no_comment'] = "[没有评论]";
$l['vote_added'] = "评价已添加";
$l['vote_updated'] = "评价已更新";
$l['vote_deleted'] = "评价已删除";
$l['vote_added_message'] = "您对这个用户的评价已经成功添加。";
$l['vote_updated_message'] = "您对这个用户的评价已经更新完毕。";
$l['vote_deleted_message'] = "您对这个用户的评价已经成功移除。";
$l['update_reputation_vote'] = "更新您对 {1} 的评价";
$l['positive'] = "正面";
$l['negative'] = "负面";
$l['neutral'] = "中性";
$l['user_comments'] = "您对 {1} 的评论:";
$l['add_vote'] = "添加评价";
$l['update_vote'] = "更新";
$l['delete_vote'] = "删除";
$l['report_vote'] = "举报";
$l['power_positive'] = "正面 ({1})";
$l['power_neutral'] = "中性";
$l['power_negative'] = "负面 ({1})";
$l['show_all'] = "显示: 所有评价";
$l['show_positive'] = "显示: 肯定的评价";
$l['show_neutral'] = "显示: 中立的评价";
$l['show_negative'] = "显示: 否定的评价";
$l['sort_updated'] = "排序: 最后更新";
$l['sort_username'] = "排序: 用户名";
$l['last_updated'] = "最近更新{1}";
$l['postrep_given'] = "给<a href=\"{1}\">{2}的帖子</a> {3}<br />评价";
$l['postrep_given_thread'] = "在 <a href=\"{1}\">{2}</a>";
$l['no_reputation_votes'] = "这个用户当前没有符合指定条件的声望评价记录。";
$l['delete_reputation_confirm'] = "您确定要删除这条评价吗?";
$l['delete_reputation_log'] = "由 {1} (UID: {2})删除的评价";
$l['reputations_disabled_group'] = "这个用户组的成员无法使用威望系统.";
$l['rate_user'] = "评价用户";
