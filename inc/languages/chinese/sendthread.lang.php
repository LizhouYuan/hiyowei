<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['nav_sendthread'] = "发送主题给好友";

$l['send_thread'] = "发送给好友";
$l['recipient'] = "收件人:";
$l['recipient_note'] = "在这里填入好友的 Email 地址。";
$l['subject'] = "邮件标题:";
$l['message'] = "内容:";
$l['image_verification'] = "图形认证";
$l['verification_subnote'] = "(不区分大小写)";
$l['verification_note'] = "请在下面文本框中填写图片中的内容，此举是为了防止垃圾机器人.";
$l['error_nosubject'] = "请您输入要发送的邮件标题。";
$l['error_nomessage'] = "请输入给您朋友的邮件内容。";

