<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['logindata_invalidpwordusername'] = "你输入了一个无效的 用户名/密码 组合<br /><br />如果你忘记了密码请 <a href=\"member.php?action=lostpw\">点击这里找回密码</a>。";
$l['logindata_invalidpwordusernameemail'] = "你输入了一个无效的email/密码组合<br /><br />如果你忘记了密码请<a href=\"member.php?action=lostpw\">点击这里找回密码</a>。";
$l['logindata_invalidpwordusernamecombo'] = "你输入了一个无效的 用户名/密码或email/密码组合<br /><br />如果你忘记了密码请 <a href=\"member.php?action=lostpw\">点击这里找回密码</a>。";

$l['logindata_regimageinvalid'] = "您输入的图片验证码不正确, 请准确地输入图片中显示的验证码。";
$l['logindata_regimagerequired'] = "请输入验证码以继续登陆过程。";
