<?php
/**
 * MyBB 1.8 English Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

$l['ucp_nav_width'] = "180";
$l['ucp_nav_menu'] = "菜单";
$l['ucp_nav_messenger'] = "私人短信(PM)";
$l['ucp_nav_compose'] = "写短信";
$l['ucp_nav_tracking'] = "追踪";
$l['ucp_nav_edit_folders'] = "编辑文件夹";
$l['ucp_nav_profile'] = "您的个人资料";
$l['ucp_nav_edit_profile'] = "编辑个人资料";
$l['ucp_nav_edit_options'] = "编辑选项";
$l['ucp_nav_change_email'] = "更换 Email";
$l['ucp_nav_change_pass'] = "修改密码";
$l['ucp_nav_change_username'] = "更换用户名";
$l['ucp_nav_edit_sig'] = "更换签名档";
$l['ucp_nav_change_avatar'] = "更换头像";
$l['ucp_nav_misc'] = "维护";
$l['ucp_nav_editlists'] = "好友/黑名单";
$l['ucp_nav_favorite_threads'] = "收藏主题";
$l['ucp_nav_subscribed_threads'] = "已订阅主题";
$l['ucp_nav_forum_subscriptions'] = "已订阅版面";
$l['ucp_nav_drafts'] = "草稿箱";
$l['ucp_nav_drafts_active'] = "<strong>已保存的草稿 ({1})</strong>";
$l['ucp_nav_notepad'] = "个人备忘录";
$l['ucp_nav_view_profile'] = "查看资料";
$l['ucp_nav_home'] = "用户面板首页";
$l['ucp_nav_usergroups'] = "用户组成员";
$l['ucp_nav_attachments'] = "附件管理";
